var SpecReporter = require('jasmine-spec-reporter').SpecReporter;
exports.config = {
	allScriptsTimeout : 11000,
	specs : [ './e2e/**/*.e2e-spec.ts' ],
	multiCapabilities : [ {
		'browserName' : 'chrome',
		'chromeOptions' : {
			'args' : [ "--no-sandbox", "--headless", "--disable-gpu" ]
		}
//	}, {
//		'browserName' : 'firefox'
	} ],
	directConnect : true,
	baseUrl : 'http://localhost:4200/',
	framework : 'jasmine',
	jasmineNodeOpts : {
		showColors : true,
		defaultTimeoutInterval : 30000,
		print : function() {
		}
	},
	useAllAngular2AppRoots : true,
	beforeLaunch : function() {
//		require('geckodriver');
		require('ts-node').register({
			project : 'e2e'
		});
	},
	onPrepare : function() {
		jasmine.getEnv().addReporter(new SpecReporter());
		require('ts-node').register({
			project : 'e2e'
		});
	}
};
