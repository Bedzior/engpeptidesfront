import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ScanErrorComponent} from './scan-error.component';

describe('ScanErrorComponent', () => {
    let component: ScanErrorComponent;
    let fixture: ComponentFixture<ScanErrorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ScanErrorComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ScanErrorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
