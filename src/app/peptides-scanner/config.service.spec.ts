import {inject, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {ScanConfigurationService} from './config.service';
import {MockAppConfigProvider} from 'test/app.config';
import {MockAuthorizationServiceProvider} from 'test/shared';

describe('ScanConfigurationService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                ScanConfigurationService,
                MockAppConfigProvider,
                MockAuthorizationServiceProvider
            ],
            imports: [HttpModule]
        });
    });

    it('should ...', inject(
        [ScanConfigurationService],
        (service: ScanConfigurationService) => {
            expect(service).toBeTruthy();
        }
    ));
});
