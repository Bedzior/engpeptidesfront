import {inject, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';
import {RouterTestingModule} from '@angular/router/testing';

import {AppConfigProvider} from 'app/common/app.config.provider';
import {ScanService} from 'app/peptides-scanner';

describe('ScanService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule, RouterTestingModule],
            providers: [AppConfigProvider, ScanService]
        });
    });

    it('should ...', inject([ScanService], (service: ScanService) => {
        expect(service).toBeTruthy();
    }));
});
