export class ScanModel {
    token: string;

    status: ScanStatus;
    parameters: string;

    created: Date;
    started: Date;
    finished: Date;

    // some details maybe?
    constructor(data: string | {}) {
        if (data instanceof Object) {
            Object.assign(this, data);
        } else {
            Object.assign(this, JSON.parse(data));
        }
        this.resolveStatus();
        return this;
    }
    resolveStatus() {
        let _status = this.status as string;
        this.status = ScanStatus[_status];
    }
}

export enum ScanStatus {
    WAITING = 'WAITING',
    RUNNING = 'RUNNING',
    FINISHED = 'FINISHED',
    ABORTED = 'ABORTED',
    FAILED = 'FAILED'
}
