import {ClassProvider} from '@angular/core';

import {ScanService} from './scan.service';
import {ScanServiceImpl} from './private/scan.service';

export const ScanServiceProvider: ClassProvider = {
    provide: ScanService,
    useClass: ScanServiceImpl
};
