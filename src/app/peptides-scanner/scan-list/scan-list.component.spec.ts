import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MatProgressSpinnerModule} from '@angular/material';
import {
    TranslateModule,
    TranslateLoader,
    TranslateService
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {ScanListComponent} from './scan-list.component';
import {MockAppConfigProvider} from 'test/app.config';
import {MockScanServiceProvider} from 'test/services';

describe('ScanListComponent', () => {
    let component: ScanListComponent;
    let fixture: ComponentFixture<ScanListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                MatProgressSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [ScanListComponent],
            providers: [
                MockAppConfigProvider,
                MockScanServiceProvider,
                TranslateService
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ScanListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
