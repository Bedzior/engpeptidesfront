import {Component, Input, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {IAppConfig, ErrorResponse} from 'app/common';
import {ScanService} from '../../scan.service';
import {ScanModel, ScanStatus} from '../../scan.model';
import {MsOutputModel} from './ms-output-model';

@Component({
    selector: 'app-scan-element',
    templateUrl: './scan-element.component.html',
    styleUrls: ['./scan-element.component.scss']
})
export class ScanElementComponent implements OnInit {
    JSON = JSON;
    ScanStatus = ScanStatus;
    @Input()
    scan: ScanModel;
    result: MsOutputModel;

    constructor(
        protected conf: IAppConfig,
        protected scanService: ScanService,
        private translate: TranslateService
    ) {}

    ngOnInit(): void {}

    view(event: Event) {
        if (this.scan.status === ScanStatus.FINISHED) {
            this.scanService.getResult(this.scan.token).then(
                (result) => {
                    this.result = result;
                },
                (err: ErrorResponse) => {
                    if (err.token) {
                        alert(
                            this.translate.instant(
                                `SCAN.ELEMENT.MESSAGE.${err.token}`
                            )
                        );
                    } else {
                        alert(
                            this.translate.instant(
                                'SCAN.ELEMENT.MESSAGE.ERR_UNEXPECTED'
                            )
                        );
                        console.error(err.message);
                    }
                }
            );
        } else {
            alert(
                this.translate.instant('SCAN.ELEMENT.MESSAGE.ERR_UNFINISHED')
            );
        }
    }
}
