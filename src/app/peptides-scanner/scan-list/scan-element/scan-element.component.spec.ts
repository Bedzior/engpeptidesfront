import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';

import {ScanModel, ScanStatus} from 'app/peptides-scanner';
import {ScanElementComponent} from 'app/peptides-scanner/scan-list/scan-element/scan-element.component';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {MockAppConfigProvider} from 'test/app.config';
import {MockScanServiceProvider} from 'test/services';

describe('ScanElementComponent', () => {
    let component: ScanElementComponent;
    let fixture: ComponentFixture<ScanElementComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [ScanElementComponent],
            providers: [
                MockAppConfigProvider,
                MockScanServiceProvider,
                TranslateService
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ScanElementComponent);
        component = fixture.componentInstance;
        component.scan = new ScanModel({
            token: '',
            status: ScanStatus.RUNNING,
            parameters: '',
            created: new Date(),
            started: new Date(),
            finished: new Date()
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
