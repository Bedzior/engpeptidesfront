import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {ErrorResponse} from 'app/common';
import {MsProteinHit} from '../ms-output-model';
import {ScanService} from '../../../scan.service';

@Component({
    selector: 'app-protein-hits',
    templateUrl: './protein-hits.component.html',
    styleUrls: ['./protein-hits.component.scss']
})
export class ProteinHitsComponent implements OnInit {
    encode = (id: string) => (id === 'N/A' ? 'unassigned' : id);
    public proteins: Array<MsProteinHit>;
    public loading: boolean = true;

    constructor(
        private service: ScanService,
        private translate: TranslateService,
        public router: Router,
        public route: ActivatedRoute
    ) {}

    ngOnInit() {
        const token: string = this.route.snapshot.params['token'];
        this.service
            .getProteins(token)
            .then(
                (proteins: {}) => {
                    let array = [];
                    for (let id in proteins) {
                        array.push(proteins[id]);
                    }
                    array.sort((a: MsProteinHit, b: MsProteinHit) => {
                        if (a.id === 'N/A') {
                            return 1;
                        } else if (b.id === 'N/A') {
                            return -1;
                        } else {
                            return a.peptidesCount > b.peptidesCount
                                ? -1
                                : a.peptidesCount < b.peptidesCount
                                ? 1
                                : 0;
                        }
                    });
                    this.proteins = array;
                    return;
                },
                (err: ErrorResponse) => {
                    if (err.token)
                        alert(
                            this.translate.instant(
                                'RESULT_DETAILS.MESSAGE.' + err.token
                            )
                        );
                    else
                        alert(
                            this.translate.instant(
                                'RESULT_DETAILS.MESSAGE.ERR_UNEXPECTED'
                            )
                        );
                    return;
                }
            )
            .then(() => {
                this.loading = false;
            });
    }
}
