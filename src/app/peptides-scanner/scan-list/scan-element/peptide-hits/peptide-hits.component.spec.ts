import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MatProgressSpinnerModule} from '@angular/material';
import {
    TranslateService,
    TranslateLoader,
    TranslateModule
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {PeptideHitsComponent} from './peptide-hits.component';
import {MockScanServiceProvider} from 'test/services';

describe('PeptideHitsComponent', () => {
    let component: PeptideHitsComponent;
    let fixture: ComponentFixture<PeptideHitsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MatProgressSpinnerModule,
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [MockScanServiceProvider, TranslateService],
            declarations: [PeptideHitsComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PeptideHitsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
