import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ErrorResponse} from 'app/common';
import {ScanService} from 'app/peptides-scanner';
import {MsPeptideHit, MsQuery} from '../ms-output-model';

@Component({
    selector: 'app-peptide-hits',
    templateUrl: './peptide-hits.component.html',
    styleUrls: ['./peptide-hits.component.scss']
})
export class PeptideHitsComponent implements OnInit {
    public peptides: Array<MsPeptideHit>;
    public loading: boolean = true;
    round = Math.round;

    constructor(
        public router: Router,
        public route: ActivatedRoute,
        private service: ScanService,
        private translate: TranslateService
    ) {}

    ngOnInit() {
        const token: string = this.route.snapshot.params['token'];
        const protein: string = this.route.snapshot.params['protein'];
        this.service
            .getPeptides(token, protein)
            .then(
                (peptides: {}) => {
                    let array = [];
                    for (let _peptide in peptides) {
                        let peptide: MsPeptideHit = peptides[_peptide];
                        peptide.queries.forEach((q: MsQuery) => {
                            q.mass = q.mass.toFixed(4);
                            q.mz = q.mz.toFixed(4);
                        });
                        peptide.queries.sort((a: MsQuery, b: MsQuery) =>
                            a.mass > b.mass ? 1 : a.mass < b.mass ? -1 : 0
                        );
                        peptide.calcMass = peptide.calcMass.toFixed(4);
                        array.push(peptide);
                    }
                    array.sort((a: MsPeptideHit, b: MsPeptideHit) =>
                        a.queriesCount > b.queriesCount
                            ? -1
                            : a.queriesCount < b.queriesCount
                            ? 1
                            : a.sequence.localeCompare(b.sequence)
                    );
                    this.peptides = array;
                    return;
                },
                (err: ErrorResponse) => {
                    if (err.token)
                        alert(
                            this.translate.instant(
                                'RESULT_DETAILS.MESSAGE.' + err.token
                            )
                        );
                    else
                        alert(
                            this.translate.instant(
                                'RESULT_DETAILS.MESSAGE.ERR_UNEXPECTED'
                            )
                        );
                    return;
                }
            )
            .then(() => (this.loading = false));
    }
}
