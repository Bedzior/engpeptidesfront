export interface MsOutputModel {
    fileFormatName: string;
    header: {};
    iTraqType: number;
    msDataFile: string;
    mergedList: Array<{}>;
    phRange: Array<number>;
    proteinsList: Array<MsProteinHit>;
    queries: Array<MsQuery>;
    warnings: Array<string>;
    errors: Array<string>;
}

export interface MsProteinHit {
    id: string;
    name: string;
    shortName: string;
    peptidesCount: number;
    score: number;
}
export interface MsPeptideHit {
    sequence: string;
    calcMass: any;
    queriesCount: number;
    queries: Array<MsQuery>;
}

export interface MsQuery {
    nr: number;
    mz: any;
    charge: number;
    mass: any;
    deltaPPM: number;
    score: number;
}
