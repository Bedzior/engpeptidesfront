import {Subscription} from 'rxjs';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {ScanModel} from '../scan.model';
import {ScanService} from '../scan.service';

@Component({
    selector: 'app-scan-list',
    templateUrl: './scan-list.component.html',
    styleUrls: ['./scan-list.component.scss']
})
export class ScanListComponent implements OnInit, OnDestroy {
    highlighted: string;
    subscription: Subscription;
    scans: Array<ScanModel>;
    loading: boolean = true;

    constructor(
        private scanService: ScanService,
        private route: ActivatedRoute
    ) {
        this.subscription = this.route.fragment.subscribe((fragment) => {
            this.highlighted = fragment;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    ngOnInit() {
        this.scanService.list().then(
            (rsp) => {
                this.scans = rsp;
                setTimeout(() => {
                    if (this.scans.length && this.highlighted) {
                        document
                            .getElementById(this.highlighted)
                            .scrollIntoView();
                    }
                }, 0);
                this.loading = false;
            },
            () => {
                this.loading = false;
            }
        );
    }
}
