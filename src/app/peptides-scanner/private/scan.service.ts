import {Observable} from 'rxjs';
import {Response} from '@angular/http';
import {Injectable, Injector} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {IAppConfig, APIService, SocketService} from 'app/common';
import {ScanService} from '../scan.service';
import {ScanModel} from '../scan.model';
import {
    MsOutputModel,
    MsPeptideHit,
    MsProteinHit
} from '../scan-list/scan-element/ms-output-model';

const endpoint = '/secured/scan';
const resultEndpoint = endpoint + '/result';
@Injectable()
export class ScanServiceImpl extends APIService implements ScanService {
    protected socketService: SocketService;

    constructor(
        conf: IAppConfig,
        injector: Injector,
        protected route: ActivatedRoute
    ) {
        super(conf, injector);
        this.socketService = injector.get(SocketService);
    }

    public listen(token: string): Observable<{}> {
        return this.socketService.subscribe('/topic/scan/' + token);
    }

    public start(form: any): Promise<Response> {
        return super.post(endpoint, form);
    }

    public list(): Promise<Array<ScanModel>> {
        return super.get(endpoint + '?finished=true');
    }

    public listRunning(): Promise<Array<ScanModel>> {
        return super.get(endpoint + '?finished=false');
    }

    public isRunningOrWaiting(token: string): Observable<{}> {
        return this.http.get(endpoint + '/' + token);
    }
    public get(token: string): Promise<ScanModel> {
        return super.get(endpoint + '/' + token).then((rsp) => {
            return new ScanModel(rsp);
        });
    }
    public getResult(token: string): Promise<MsOutputModel> {
        return super.get(resultEndpoint + '/' + token);
    }
    public cancel(token: string): void {
        return this.socketService.publish('/app/scan/' + token, {
            command: 'cancel'
        });
    }
    public getProteins(token: string): Promise<Array<MsProteinHit>> {
        return super.get(resultEndpoint + '/' + token + '/proteins');
    }
    public getPeptides(
        token: string,
        protein: string
    ): Promise<Array<MsPeptideHit>> {
        return super.get(resultEndpoint + '/' + token + '/proteins/' + protein);
    }
}
