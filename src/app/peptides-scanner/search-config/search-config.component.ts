import {Subscription} from 'rxjs';
import {
    Component,
    OnInit,
    OnDestroy,
    Inject,
} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {
    FormGroup,
    FormBuilder,
    Validators,
    FormArray
} from '@angular/forms';

import * as Sorters from 'app/common/sorters';
import {ScanConfigurationService} from '../config.service';
import {SearchConfig} from '../search-config/search-config';
import {ScanService} from '../scan.service';

import {
    UserModel,
    AuthorizationService
} from 'app/common';
import {tooltipSwitch, placeholderSwitch} from 'app/common/animations';
import { MyValidators } from '../../common/forms/my-validators';
import { TranslateService } from '@ngx-translate/core';
import { ErrorResponse } from '../../common/responses';

@Component({
    selector: 'app-search-config',
    animations: [placeholderSwitch, tooltipSwitch],
    templateUrl: './search-config.component.html',
    styleUrls: ['./search-config.component.scss']
})
export class SearchConfigComponent implements OnInit, OnDestroy {
    protected user: UserModel;

    public loading: boolean = true;

    public submitting: boolean = false;

    protected sources: {} = {
        enzymes: [],
        units: [],
        instruments: [],
        ptms: [],
        databases: []
    };

    private sub: Subscription;
    private firstLoad = true;

    public formData: Object[];
    public form: FormGroup;
    /***
     * Whether the component was initialised with <i>advanced</i> URL parameter;<br/>
     * Indicates config component loading schema.
     */
    public configType: string;

    protected config: SearchConfig = new SearchConfig();

    constructor(
        @Inject(FormBuilder) private fb: FormBuilder,
        private configService: ScanConfigurationService,
        private scanService: ScanService,
        private authService: AuthorizationService,
        private router: Router,
        private route: ActivatedRoute,
        public translate: TranslateService
    ) {
        this.user = authService.getUser();
        this.config.setUserData(this.user);
        this.sub = this.route.params.subscribe((params) => {
            this.configType = params['configType'];
            console.log(
                'User switched to',
                this.configType,
                'search configuration'
            );
            this.loading = true;
            this.configService
                .getOptions(this.configType === 'advanced')
                .then((src: {}) => {
                    this.setSources(src);
                    this.loading = false;
                    window.dispatchEvent(new Event('resize'));
                })
                .catch((err) => {
                    this.loading = false;
                    if (err.message === 'Unauthorized' || err.status === 401) {
                        this.router.navigate(['/logout']);
                    } else if (!authService.loggedIn) {
                        this.router.navigate(['/logout']);
                    } else {
                        console.log(
                            'Error retrieving available configuration',
                            err
                        );
                    }
                });
        });
    }

    isArray(control: any): boolean {
        return control instanceof FormArray;
    }

    ngOnInit() {
        this.form = this.fb.group({
            user: this.fb.group({
                name: this.fb.control(this.user.getLogin()),
                email: this.fb.control(this.user.getEmail())
            }),
            data: this.fb.group({
                database: this.fb.control(this.config.database, Validators.required)
            }),
            bio: this.fb.group({
                enzyme: [this.config.enzyme, Validators.required],
                missedCleavage: this.fb.control(
                    this.config.missedCleavage,
                    Validators.required
                ),
                peptideTolerance: this.fb.control(
                    this.config.peptideTolerance,
                    Validators.required
                ),
                peptideToleranceUnits: this.fb.control(
                    this.config.peptideToleranceUnits,
                    Validators.required
                ),
                peakDetection: this.fb.control(
                    this.config.peakDetection,
                    Validators.compose([
                        this.isAdvanced()
                            ? Validators.required
                            : Validators.nullValidator,
                        Validators.min(0),
                        Validators.max(2)
                    ])
                ),
                tandemMassTolerance: this.fb.control(
                    this.config.tandemMassTolerance,
                    Validators.required
                ),
                tandemMassToleranceUnits: this.fb.control(
                    this.config.tandemMassToleranceUnits,
                    Validators.required
                ),
                peptideCharge: this.fb.control(
                    this.config.peptideCharge,
                    Validators.compose([
                        Validators.required,
                        MyValidators.listOfNumbers
                    ])
                )
            }),
            modifications: this.fb.group({
                fixModifications: this.config.fixModifications,
                varModifications: this.config.varModifications
            }),
            other: this.fb.group({
                instrument: this.fb.control(this.config.instrument, Validators.required),
                files: this.fb.array([
                    this.fb.control(this.config.files)
                ], Validators.minLength(1))
            })
        });
        this.formData = [
            {
                name: 'user',
                controls: [
                    {
                        name: 'name',
                        readonly: true
                    },
                    {
                        name: 'email',
                        readonly: true
                    }
                ]
            },
            {
                name: 'data',
                controls: [
                    {
                        name: 'database',
                        source: this.sources['databases'],
                        type: 'select'
                    }
                ]
            },
            {
                name: 'bio',
                controls: [
                    {
                        name: 'enzyme',
                        type: 'select',
                        source: this.sources['enzymes']
                    },
                    {
                        name: 'missedCleavage',
                        value: this.config.missedCleavage
                    },
                    {
                        name: 'peptideTolerance',
                        type: 'number',
                        step: 0.1,
                        value: this.config.peptideTolerance
                    },
                    {
                        name: 'peptideToleranceUnits',
                        placeholder: 'Peptide tol. units',
                        type: 'select',
                        value: this.config.peptideToleranceUnits,
                        source: this.sources['units']
                    },
                    {
                        name: 'tandemMassTolerance',
                        type: 'number',
                        step: 0.1,
                        value: this.config.tandemMassTolerance
                    },
                    {
                        name: 'tandemMassToleranceUnits',
                        type: 'select',
                        value: this.config.tandemMassToleranceUnits,
                        source: this.sources['units']
                    },
                    {
                        name: 'peakDetection',
                        type: 'number',
                        advanced: true,
                        value: this.config.peakDetection
                    },
                    {
                        name: 'peptideCharge',
                        type: 'text',
                        value: this.config.peptideCharge
                    }
                ]
            },
            {
                name: 'modifications',
                source: this.sources['ptms'],
                controls: [
                    {
                        name: 'fixModifications',
                        type: 'joint-select',
                        with: 'varModifications',
                        attr: 'name'
                    },
                    {
                        name: 'varModifications',
                        placeholder: '',
                        type: 'joint-select',
                        with: 'fixModifications',
                        attr: 'name'
                    }
                ]
            },
            {
                name: 'other',
                controls: [
                    {
                        name: 'instrument',
                        type: 'select',
                        source: this.sources['instruments']
                    },
                    {
                        name: 'files',
                        type: 'several-files'
                    }
                ]
            }
        ];
    }

    private setSources(src: {}): void {
        if (this.firstLoad) {
            this.sources['units'].length = 0;
            for (let unit of src['mmdUnits']) {
                unit.name = this.translate.instant('SEARCH_CONFIG.FORM.UNITS.' + unit.short);
            }
            Array.prototype.push.apply(this.sources['units'], src['mmdUnits']);
            this.firstLoad = false;
        }
        for (let source of ['enzymes', 'instruments', 'ptms', 'databases']) {
            if (src[source]) {
                if (source !== 'ptms') {
                    src[source].sort(Sorters.backendValues);
                } else if (this.sources[source].length) {
                    continue; // don't update for PTMs
                }
                this.sources[source].length = 0;
                Array.prototype.push.apply(this.sources[source], src[source]);
            }
        }
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    public submit(event: Event): boolean {
        event.preventDefault();
        if (this.form.valid) {
            this.submitting = true;
            if (!this.isAdvanced()) {
                delete this.form.value['bio']['peakDetection'];
            }
            this.scanService
                .start(this.form.value)
                .then((result: {}) => {
                    this.router.navigate(['/running/' + result['token']]);
                }, (err: ErrorResponse) => {
                    alert(
                        this.translate.instant('SEARCH_CONFIG.MESSAGE.ERR_PREFIX') +
                        this.translate.instant('SEARCH_CONFIG.MESSAGE.' + err.token)
                    );
                    this.submitting = false;
                });
        }
        return false;
    }
    public isAdvanced(): boolean {
        return this.configType === 'advanced';
    }
}
