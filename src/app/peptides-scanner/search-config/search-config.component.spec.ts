import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';

import {SearchConfigComponent} from 'app/peptides-scanner';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {
    MockAuthorizationServiceProvider,
    MockScanConfigurationServiceProvider,
    MockScanServiceProvider
} from 'test/services';

describe('SearchConfigComponent', () => {
    let component: SearchConfigComponent;
    let fixture: ComponentFixture<SearchConfigComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                HttpModule,
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [
                FormBuilder,
                MockAuthorizationServiceProvider,
                MockScanServiceProvider,
                MockScanConfigurationServiceProvider,
                TranslateService
            ],
            declarations: [SearchConfigComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    it('should create with a user in session', () => {
        fixture = TestBed.createComponent(SearchConfigComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    afterEach(() => {});
});
