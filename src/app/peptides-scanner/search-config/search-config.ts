import {UserModel} from 'app/common';

export class SearchConfig {
    private name: string;
    private email: string;

    public database: string;

    public enzyme: string;
    public missedCleavage: number = 1;
    public fixModifications: Array<string> = [];
    public varModifications: Array<string> = [];
    public peptideTolerance: number = 1;
    public peptideToleranceUnits: string = 'ppm';
    public peakDetection: number = 0;
    public tandemMassTolerance: number = 1;
    public tandemMassToleranceUnits: string = 'ppm';

    public peptideCharge: number = 0;

    public instrument: string;
    public files: Array<string> = [];

    public setUserData(user: UserModel): void {
        this.name = user.getLogin();
        this.email = user.getEmail();
    }
}
