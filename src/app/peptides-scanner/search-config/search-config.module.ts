import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {BrowserModule, Title} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CookieService} from 'angular2-cookie';
import {FileUploadModule} from 'ng2-file-upload';
// Components
import {
    LoggedInGuard,
    PeptideMultiselectComponent,
    PeptideSelectComponent,
    ReactiveInputComponent
} from 'app/common';
import {AppCommonModule} from 'app/common/common.module';
import {SettingsComponent} from 'app/user';
import {ScanConfigurationServiceProvider} from '../config.service';
import {RunningListComponent} from '../running/running-list.component';
import {RunningComponent} from '../running/running.component';
import {ScanErrorComponent} from '../scan-error/scan-error.component';
import {PeptideHitsComponent} from '../scan-list/scan-element/peptide-hits/peptide-hits.component';
import {ProteinHitsComponent} from '../scan-list/scan-element/protein-hits/protein-hits.component';
import {ScanElementComponent} from '../scan-list/scan-element/scan-element.component';
import {ScanListComponent} from '../scan-list/scan-list.component';
import {ScanServiceProvider} from '../scan.service.provider';
import {SearchConfigComponent} from './search-config.component';

// Guards and services

export const configRoutes: Routes = [
    {
        path: 'config/:configType',
        component: SearchConfigComponent,
        data: {
            title: 'SEARCH_CONFIG.CONFIG'
        },
        canActivate: [LoggedInGuard]
    },
    {
        path: 'config',
        redirectTo: '/config/simple',
        canActivate: [LoggedInGuard]
    },
    {
        path: 'settings',
        component: SettingsComponent,
        data: {
            title: 'SEARCH_CONFIG.SETTINGS'
        },
        canActivate: [LoggedInGuard]
    },
    {
        path: 'scanError',
        component: ScanErrorComponent,
        data: {
            title: 'SEARCH_CONFIG.SCAN_ERROR'
        },
        canActivate: [LoggedInGuard]
    },
    {
        path: 'running',
        component: RunningListComponent,
        data: {
            title: 'SEARCH_CONFIG.RUNNING_LIST'
        },
        canActivate: [LoggedInGuard]
    },
    {
        path: 'running/:token',
        component: RunningComponent,
        data: {
            title: 'SEARCH_CONFIG.RUNNING_ITEM'
        },
        canActivate: [LoggedInGuard]
    },
    {
        component: ScanListComponent,
        path: 'scans',
        data: {
            title: 'SEARCH_CONFIG.SCAN_LIST'
        },
        canActivate: [LoggedInGuard],
        canActivateChild: [LoggedInGuard],
        children: [
            {
                path: ':token',
                component: ScanElementComponent,
                data: {
                    title: 'SEARCH_CONFIG.SCAN_ITEM'
                }
            }
        ]
    },
    {
        path: 'scans/:token/proteins',
        component: ProteinHitsComponent,
        data: {
            title: 'SEARCH_CONFIG.PROTEINS'
        }
    },
    {
        path: 'scans/:token/proteins/:protein',
        component: PeptideHitsComponent,
        data: {
            title: 'SEARCH_CONFIG.PEPTIDES'
        }
    }
];

@NgModule({
    imports: [
        TranslateModule.forChild(),
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        AppCommonModule,
        FileUploadModule,
        RouterModule.forChild(configRoutes),
        MatProgressSpinnerModule
    ],
    exports: [RouterModule],
    declarations: [
        ScanErrorComponent,
        SearchConfigComponent,
        SettingsComponent,
        PeptideSelectComponent,
        PeptideMultiselectComponent,
        RunningComponent,
        RunningListComponent,
        ScanListComponent,
        ScanElementComponent,
        ProteinHitsComponent,
        PeptideHitsComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [SearchConfigComponent, ReactiveInputComponent],
    providers: [
        Title,
        CookieService,
        LoggedInGuard,
        ScanConfigurationServiceProvider,
        ScanServiceProvider
    ]
})
export class SearchConfigModule {}
