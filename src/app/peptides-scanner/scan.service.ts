import {Observable} from 'rxjs';
import {Response} from '@angular/http';

import {ScanModel} from './scan.model';
import {
    MsOutputModel,
    MsProteinHit,
    MsPeptideHit
} from './scan-list/scan-element/ms-output-model';

export abstract class ScanService {
    abstract listen(token: string): Observable<{}>;
    abstract start(form: any): Promise<Response>;
    /**
     * Lists all finished peptide scans
     */
    abstract list(): Promise<Array<ScanModel>>;
    /**
     * Lists all running peptide scans
     */
    abstract listRunning(): Promise<Array<ScanModel>>;
    abstract isRunningOrWaiting(token: string): Observable<{}>;
    abstract get(token: string): Promise<ScanModel>;
    abstract getResult(token: string): Promise<MsOutputModel>;
    abstract getProteins(token: string): Promise<Array<MsProteinHit>>;
    abstract getPeptides(
        token: string,
        protein: string
    ): Promise<Array<MsPeptideHit>>;
    abstract cancel(token: string): void;
}
