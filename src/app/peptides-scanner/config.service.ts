import {Injectable, Injector, ClassProvider} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {IAppConfig, APIService} from 'app/common';

const endpoint = '/secured/config';

export abstract class ScanConfigurationService {
    abstract getOptions(advanced?: boolean): Promise<{}>;
}

@Injectable()
export class ScanConfigurationServiceImpl extends APIService
    implements ScanConfigurationService {
    constructor(
        conf: IAppConfig,
        injector: Injector,
        private translate: TranslateService) {
        super(conf, injector);
    }

    public getOptions(advanced?: boolean): Promise<{}> {
        let url = endpoint + '/' + (advanced ? 'advanced' : 'simple');
        let service = this;
        return new Promise((resolve, reject) => {
            service.get(url).then(
                (rsp) => {
                    resolve(rsp);
                },
                (err) => {
                    reject(err);
                    alert(this.translate.instant('ADMIN.CONFIG.MESSAGES.ERR_RETRIEVE'));
                }
            );
        });
    }
}

export const ScanConfigurationServiceProvider: ClassProvider = {
    provide: ScanConfigurationService,
    useClass: ScanConfigurationServiceImpl
};
