import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {ScanModel} from '../scan.model';
import {ScanService} from '../scan.service';
import {ErrorResponse} from '../../common/responses';

@Component({
    selector: 'app-running-list',
    templateUrl: './running-list.component.html',
    styleUrls: ['./running-list.component.scss']
})
export class RunningListComponent implements OnInit {
    loading: boolean = true;
    scans: Array<ScanModel>;

    constructor(
        private scanService: ScanService,
        private translate: TranslateService) {}

    ngOnInit() {
        this.refresh();
    }

    refresh() {
        this.loading = true;
        this.scanService
            .listRunning()
            .then((rsp: [ScanModel]) => {
                this.scans = rsp;
                this.loading = false;
                window.dispatchEvent(new Event('resize'));
            }, (err: ErrorResponse) => {
                alert(this.translate.instant('RUNNING.LIST.MESSAGE.' + err.token));
            });
    }
}
