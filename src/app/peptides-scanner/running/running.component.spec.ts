import {MatProgressSpinnerModule} from '@angular/material';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateModule,
    TranslateLoader,
    TranslateService
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {RunningComponent} from './running.component';
import {MockScanServiceProvider} from 'test/services';
import {ScanService, ScanModel, ScanStatus} from 'app/peptides-scanner';

describe('RunningComponent', () => {
    let component: RunningComponent;
    let fixture: ComponentFixture<RunningComponent>;
    let scanService: ScanService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MatProgressSpinnerModule,
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [MockScanServiceProvider, TranslateService],
            declarations: [RunningComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        scanService = TestBed.get(ScanService);
        let scan = new ScanModel({
            token: '',
            status: ScanStatus.RUNNING,
            parameters: '',
            created: new Date(),
            started: new Date(),
            finished: new Date()
        });
        spyOn(scanService, 'get').and.returnValue(
            new Promise((resolve, reject) => {
                resolve(scan);
            })
        );
        fixture = TestBed.createComponent(RunningComponent);
        component = fixture.componentInstance;
        component.scan = scan;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
