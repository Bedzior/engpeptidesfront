import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatProgressSpinnerModule} from '@angular/material';
import {
    TranslateService,
    TranslateModule,
    TranslateLoader
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {RunningListComponent} from './running-list.component';
import {MockScanServiceProvider} from 'test/services';
import {ScanService} from 'app/peptides-scanner';

describe('RunningListComponent', () => {
    let component: RunningListComponent;
    let fixture: ComponentFixture<RunningListComponent>;
    let scanService: ScanService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                MatProgressSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [RunningListComponent],
            providers: [MockScanServiceProvider, TranslateService],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RunningListComponent);
        component = fixture.componentInstance;
        scanService = TestBed.get(ScanService);
        spyOn(scanService, 'list').and.returnValue(
            new Promise((resolve, reject) => {
                resolve([]);
            })
        );
        spyOn(scanService, 'listRunning').and.returnValue(
            new Promise((resolve, reject) => {
                resolve([]);
            })
        );
        fixture.detectChanges();
    });
    afterEach(() => {
        scanService = null;
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
