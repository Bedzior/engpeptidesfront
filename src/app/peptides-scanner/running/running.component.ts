import {Subscription} from 'rxjs';
import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {ScanService} from '../scan.service';
import {ScanModel, ScanStatus} from '../scan.model';
import {ErrorResponse} from 'app/common';

@Component({
    selector: 'app-running',
    templateUrl: './running.component.html',
    styleUrls: ['./running.component.scss']
})
export class RunningComponent implements OnInit, OnDestroy {
    sub: Subscription;
    routeParamChanges: Subscription;

    finished: boolean = false;

    @Input()
    token: string;

    messages: Array<string> = [];
    scan: ScanModel;
    ScanStatus = ScanStatus;
    JSON = JSON;

    constructor(
        private route: ActivatedRoute,
        private scanService: ScanService,
        private translate: TranslateService
    ) {}

    ngOnInit() {
        this.routeParamChanges = this.route.params.subscribe((params) => {
            this.token = params['token'];
            this.scanService.get(this.token).then(
                (rsp) => {
                    this.scan = rsp;
                },
                (err) => {
                    console.error(
                        'Error retrieving scan status: +' + this.token,
                        err
                    );
                }
            );
            this.sub = this.scanService.listen(this.token).subscribe(
                (rsp) => {
                    if (typeof rsp['status'] === 'string') {
                        this.messages.push(rsp['status']);
                    }
                    if (rsp['abort']) {
                        this.finished = true;
                        this.scan.status = ScanStatus.ABORTED;
                        this.messages.push('Scanning was aborted!');
                    } else if (
                        rsp['finish'] &&
                        this.scan.status !== ScanStatus.ABORTED
                    ) {
                        this.finished = true;
                        this.scan.status = ScanStatus.FINISHED;
                        this.messages.push('Finished scanning!');
                    }
                },
                (err: ErrorResponse) => {
                    console.log(err.message);
                    alert(
                        this.translate.instant(
                            'RUNNING.ELEMENT.MESSAGE.' + err.token
                        )
                    );
                },
                () => {
                    this.messages.push('Finished scanning!');
                    this.finished = true;
                }
            );
        });
    }

    ngOnDestroy(): void {
        this.sub.unsubscribe();
        this.routeParamChanges.unsubscribe();
    }

    cancel(event) {
        this.scanService.cancel(this.token);
    }
}
