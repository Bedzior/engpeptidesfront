import {NO_ERRORS_SCHEMA} from '@angular/core';
import {ComponentFixture, TestBed} from '@angular/core/testing';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';

import {SettingsComponent} from 'app/user';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {
    MockAuthorizationServiceProvider,
    MockIdentityServiceProvider
} from 'test/services';

describe('SettingsComponent', () => {
    let component: SettingsComponent;
    let fixture: ComponentFixture<SettingsComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                FormBuilder,
                MockAuthorizationServiceProvider,
                MockIdentityServiceProvider,
                TranslateService
            ],
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                ReactiveFormsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [SettingsComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    });

    it('should only create with a user in session', () => {
        fixture = TestBed.createComponent(SettingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        expect(component).toBeTruthy();
    });

    afterEach(() => {});
});
