import {Subscription} from 'rxjs';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {
    AuthorizationService,
    IdentityService,
    MyValidators,
    placeholderSwitch,
    UserModel
} from 'app/common';

@Component({
    selector: 'app-settings',
    animations: [placeholderSwitch],
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
    private sub: Subscription;

    public form: FormGroup;

    public user: UserModel;

    constructor(
        private fb: FormBuilder,
        private idService: IdentityService,
        private authService: AuthorizationService
    ) {
        this.user = authService.getUser();
        this.form = fb.group({
            name: [this.user.getLogin(), Validators.required],
            email: [this.user.getEmail(), MyValidators.email]
        });
    }

    ngOnInit() {
        let original: UserModel = this.authService.getUser();
        this.sub = this.form.valueChanges.subscribe((ev) => {
            let changed = false;
            for (let prop of ev) {
                if (ev[prop] !== original[prop]) {
                    changed = true;
                    break;
                }
            }
            if (!changed) {
                this.form.markAsPristine();
            }
        });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    public submit(event: Event): boolean {
        event.preventDefault();
        if (this.form.valid) {
            this.idService.update(this.form.value);
        }
        return false;
    }
}
