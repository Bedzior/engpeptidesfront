import {
    ControlPanelItem,
    SeparatorItem
} from '../../common/control-panel/control-panel-item';

export const elements: Array<ControlPanelItem | SeparatorItem> = [{
        id: 'running',
        route: '/running',
        icon: 'fa-question-circle'
    }, {
        id: 'scans',
        route: '/scans',
        icon: 'fa-check-circle'
    }, {
        id: 'config',
        icon: 'fa-plus-circle',
        children: [{
                id: 'simple',
                route: '/config/simple',
                icon: 'fa-cog'
            }, {
                id: 'advanced',
                route: '/config/advanced',
                icon: 'fa-cogs'
            }
        ]
    }, {
        id: 'user',
        route: '/settings',
        icon: 'fa-user'
    }, new SeparatorItem(), {
        id: 'logout',
        route: '/logout',
        icon: 'fa-sign-out'
    }
];
