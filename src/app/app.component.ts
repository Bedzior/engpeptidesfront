import { Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Event as RouterEvent, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { AuthorizationService, floatboxCover, floatboxExit } from 'app/common';
import * as admin from './admin/admin-control-panel/admin-control-panel';
import * as user from './user/user-control-panel/user-control-panel';

@Component({
    selector: 'app-root',
    animations: [floatboxExit, floatboxCover],
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
    public hideContents: boolean = false;
    public language: string;
    public mode: string = 'user';

    public loading: boolean = true;
    public adminElements: Array<{}> = admin.elements;
    public userElements: Array<{}> = user.elements;

    private subscription: Subscription;

    constructor(
        public translate: TranslateService,
        public router: Router,
        public auth: AuthorizationService
    ) {
        this.translate.langs = ['pl', 'en'];
        router.events.subscribe((e: RouterEvent) => {
            let next: boolean = e instanceof NavigationStart;
            if (this.loading !== next) {
                this.loading = next;
            }
        });
    }

    async ngOnInit() {
        this.auth.loggedIn = await this.auth.checkSession();
        let browserLanguage = this.translate.getBrowserLang();
        this.language =
            this.translate.langs.indexOf(browserLanguage) >= 0
                ? browserLanguage
                : 'en';
        let mode = this.router.url.startsWith('/admin') ? 'admin' : 'user';
        this.modeChanged(mode);
        this.subscription = this.router.events
            .filter((event) => {
                return event instanceof NavigationEnd;
            })
            .subscribe((event: NavigationEnd) => {
                let mode = event.urlAfterRedirects.startsWith('/admin')
                    ? 'admin'
                    : 'user';
                this.modeChanged(mode);
            });
    }
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
    public modeChanged(mode) {
        if (this.mode !== mode) {
            this.mode = mode;
            if (
                !this.router.url.startsWith('/admin') &&
                this.mode === 'admin'
            ) {
                this.router.navigate(['/admin']);
            } else if (
                this.router.url.startsWith('/admin') &&
                this.mode === 'user'
            ) {
                this.router.navigate(['/config']);
            }
        }
    }
}
