import {Injectable, Injector, ClassProvider} from '@angular/core';
import {APIService, IAppConfig} from 'app/common';
import {ConfigurationValueModel} from '..';

const endpoint = '/secured/configuration';

export abstract class AdminConfigurationService {
    abstract listConfiguration(): Promise<Array<ConfigurationValueModel>>;
    abstract getConfig(name: string): Promise<ConfigurationValueModel>;
    abstract updateConfig(
        config: ConfigurationValueModel
    ): Promise<ConfigurationValueModel>;
    abstract updateConfigs(
        config: Array<ConfigurationValueModel>
    ): Promise<Array<ConfigurationValueModel>>;
}

@Injectable()
export class AdminConfigurationServiceImpl extends APIService
    implements AdminConfigurationService {
    constructor(protected conf: IAppConfig, protected injector: Injector) {
        super(conf, injector);
    }
    public listConfiguration(): Promise<Array<ConfigurationValueModel>> {
        return this.get(endpoint);
    }
    public getConfig(name: string): Promise<ConfigurationValueModel> {
        return this.get(endpoint + '/' + name);
    }
    public updateConfig(
        config: ConfigurationValueModel
    ): Promise<ConfigurationValueModel> {
        return this.put(endpoint + '/' + config.name, config);
    }
    public updateConfigs(
        config: Array<ConfigurationValueModel>
    ): Promise<Array<ConfigurationValueModel>> {
        return this.put(endpoint, config);
    }
}

export const AdminConfigurationServiceProvider: ClassProvider = {
    provide: AdminConfigurationService,
    useClass: AdminConfigurationServiceImpl
};
