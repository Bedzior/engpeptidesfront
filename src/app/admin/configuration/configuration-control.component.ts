import {Component, OnInit, Input} from '@angular/core';
import {ConfigurationValueModel} from './configuration-value-model';

@Component({
    selector: 'app-configuration-control',
    templateUrl: './configuration-control.component.html',
    styleUrls: ['./configuration-control.component.scss']
})
export class ConfigurationControlComponent implements OnInit {
    @Input() model: ConfigurationValueModel;

    constructor() {}

    ngOnInit() {}
}
