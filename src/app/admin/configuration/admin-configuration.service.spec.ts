import {TestBed, inject} from '@angular/core/testing';

import {AdminConfigurationService} from './admin-configuration.service';

describe('AdminConfigurationService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [AdminConfigurationService]
        });
    });

    it('should be created', inject(
        [AdminConfigurationService],
        (service: AdminConfigurationService) => {
            expect(service).toBeTruthy();
        }
    ));
});
