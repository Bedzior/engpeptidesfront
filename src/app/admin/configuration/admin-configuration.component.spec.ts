import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatProgressSpinnerModule} from '@angular/material';

import {AdminConfigurationComponent} from './admin-configuration.component';
import {AdminConfigurationService, ConfigurationValueModel} from 'app/admin';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {
    MockAdminServiceProvider,
    MockAdminConfigurationServiceProvider
} from 'test/services';

describe('AdminConfigurationComponent', () => {
    let component: AdminConfigurationComponent;
    let fixture: ComponentFixture<AdminConfigurationComponent>;
    let adminConfigService: AdminConfigurationService;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AdminConfigurationComponent],
            imports: [
                MatProgressSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [
                MockAdminServiceProvider,
                MockAdminConfigurationServiceProvider,
                TranslateService
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        adminConfigService = TestBed.get(AdminConfigurationService);
        spyOn(adminConfigService, 'listConfiguration').and.returnValue(
            new Promise((resolve, reject) => {
                let result: ConfigurationValueModel[];
                result.push(
                    new ConfigurationValueModel(
                        'name',
                        'type',
                        'description',
                        'value'
                    )
                );
                resolve(result);
            })
        );
        fixture = TestBed.createComponent(AdminConfigurationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
