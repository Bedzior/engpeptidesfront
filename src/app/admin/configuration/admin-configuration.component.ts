import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {ErrorResponse} from 'app/common';
import {ConfigurationUpdateValueModel} from '../configuration/configuration-update-value-model';
import {AdminConfigurationService} from './admin-configuration.service';
import {ConfigurationValueModel} from './configuration-value-model';

@Component({
    selector: 'app-admin-configuration',
    templateUrl: './admin-configuration.component.html',
    styleUrls: ['./admin-configuration.component.scss']
})
export class AdminConfigurationComponent implements OnInit {
    loading: boolean = true;

    configurations: Array<ConfigurationValueModel>;

    constructor(
        private configService: AdminConfigurationService,
        private translate: TranslateService
    ) {}

    ngOnInit() {
        this.configService.listConfiguration().then(
            (data: Array<ConfigurationValueModel>) => {
                this.configurations = data;
                this.loading = false;
            },
            (e: ErrorResponse) => {
                this.loading = false;
                console.log('The current configuration could not be retrieved');
                alert(
                    this.translate.instant('ADMIN.CONFIG.MESSAGES.' + e.token)
                );
            }
        );
    }

    submit(event: Event) {
        if (event) {
            event.stopPropagation();
        }
        let configurations = [];
        for (let config of this.configurations) {
            configurations.push(
                new ConfigurationUpdateValueModel(config.name, config.value)
            );
        }
        this.loading = true;
        this.configService.updateConfigs(configurations).then(
            (data: Array<ConfigurationValueModel>) => {
                this.configurations = data;
                this.loading = false;
                alert(this.translate.instant('ADMIN.CONFIG.MESSAGES.SUCCESS'));
            },
            (e: ErrorResponse) => {
                console.log('Configuration has not been updated: ' + e.message);
                alert(
                    this.translate.instant('ADMIN.CONFIG.MESSAGES.ERR_INTRO') +
                        ' ' +
                        this.translate.instant(
                            'ADMIN.CONFIG.MESSAGES.' + e.token
                        )
                );
            }
        );
        return false;
    }
}
