export class ConfigurationValueModel {
    constructor(
        public name: string,
        public type: string,
        public description: string,
        public value: any
    ) {}

    public getName(): string {
        return this.name;
    }
    public getDescription(): string {
        return this.description;
    }
    public getValue() {
        return this.value;
    }
    public getType() {
        return this.type;
    }
    public setValue(value: any): void {
        this.value = value;
    }
}
