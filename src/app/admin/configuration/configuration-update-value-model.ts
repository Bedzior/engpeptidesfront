export class ConfigurationUpdateValueModel {
    constructor(public name?: string, public value?: any) {}

    public getName(): string {
        return this.name;
    }
    public getValue() {
        return this.value;
    }
    public setValue(value: any): void {
        this.value = value;
    }
}
