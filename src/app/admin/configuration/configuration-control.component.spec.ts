import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ConfigurationControlComponent} from 'app/admin';
import {ConfigurationValueModel} from '..';

describe('ConfigurationControlComponent', () => {
    let component: ConfigurationControlComponent;
    let fixture: ComponentFixture<ConfigurationControlComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FormsModule, ReactiveFormsModule],
            declarations: [ConfigurationControlComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfigurationControlComponent);
        component = fixture.componentInstance;
        component.model = new ConfigurationValueModel('', '', '', 1); // TODO individual model for each test
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
