import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatProgressSpinnerModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';

import {AdminComponent} from 'app/admin';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {
    MockAuthorizationServiceProvider,
    MockSocketServiceProvider,
    MockAdminServiceProvider
} from 'test/services';

describe('AdminComponent', () => {
    let component: AdminComponent;
    let fixture: ComponentFixture<AdminComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [
                MockAuthorizationServiceProvider,
                MockSocketServiceProvider,
                MockAdminServiceProvider,
                TranslateService
            ],
            imports: [
                RouterTestingModule,
                MatProgressSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [AdminComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AdminComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
