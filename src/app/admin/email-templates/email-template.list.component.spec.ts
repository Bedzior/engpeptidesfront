import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EmailTemplateListComponent} from './email-template.list.component';
import {MockEmailTemplateServiceProvider} from 'test/services';
import {MatProgressSpinnerModule} from '@angular/material';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('EmailTemplate.ListComponent', () => {
    let component: EmailTemplateListComponent;
    let fixture: ComponentFixture<EmailTemplateListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [MockEmailTemplateServiceProvider],
            imports: [MatProgressSpinnerModule],
            declarations: [EmailTemplateListComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EmailTemplateListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
