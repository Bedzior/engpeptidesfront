import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {NgxEditorModule} from 'ngx-editor';
import {
    TranslateService,
    TranslateModule,
    TranslateLoader
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {MockEmailTemplateServiceProvider} from 'test/services';
import {EmailTemplateComponent} from './email-template.component';
import {EmailTemplateModel} from 'app/admin';

describe('EmailTemplateComponent', () => {
    let component: EmailTemplateComponent;
    let fixture: ComponentFixture<EmailTemplateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [MockEmailTemplateServiceProvider, TranslateService],
            imports: [
                NgxEditorModule,
                ReactiveFormsModule,
                FormsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [EmailTemplateComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EmailTemplateComponent);
        component = fixture.componentInstance;
        component.model = new EmailTemplateModel({
            id: 2,
            name: 'SomeTemplate',
            type: 'html',
            body: '<p>Hello, world!</p'
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
