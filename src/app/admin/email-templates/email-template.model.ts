export class EmailTemplateModel {
    private id: number;
    public name: string;
    public type: string;
    public body: string;
    public constructor(json: string | Object) {
        if (json instanceof Object) {
            Object.assign(this, json);
        } else {
            Object.assign(this, JSON.parse(json));
        }
        return this;
    }
    public getID() {
        return this.id;
    }
    public getName() {
        return this.name;
    }
    public getType() {
        return this.type;
    }
    public getBody() {
        return this.body;
    }
}
