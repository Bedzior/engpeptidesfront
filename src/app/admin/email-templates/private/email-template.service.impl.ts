import {Injectable, Injector} from '@angular/core';

import {IAppConfig, APIService} from 'app/common';
import {EmailTemplateService} from '../email-template.service';
import {EmailTemplateModel} from '../email-template.model';

const endpoint = '/secured/admin/templates';

@Injectable()
export class EmailTemplateServiceImpl extends APIService
    implements EmailTemplateService {
    constructor(protected conf: IAppConfig, protected injector: Injector) {
        super(conf, injector);
    }

    public list(): Promise<Array<EmailTemplateModel>> {
        return super.get(endpoint);
    }

    public getSingle(id: number): Promise<EmailTemplateModel> {
        return super.get(endpoint + '/' + id);
    }

    public update(item: EmailTemplateModel): Promise<EmailTemplateModel> {
        return super.put(endpoint + '/' + item['id'], item);
    }
}
