import {ClassProvider} from '@angular/core';

import {EmailTemplateService} from './email-template.service';
import {EmailTemplateServiceImpl} from './private/email-template.service.impl';

export const EmailTemplateServiceProvider: ClassProvider = {
    provide: EmailTemplateService,
    useClass: EmailTemplateServiceImpl
};
