import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {EmailTemplateModel} from './email-template.model';
import {EmailTemplateService} from './email-template.service';
import {ErrorResponse} from 'app/common';

@Component({
    selector: 'app-email-template',
    templateUrl: './email-template.component.html',
    styleUrls: ['./email-template.component.scss']
})
export class EmailTemplateComponent implements OnInit {
    @Input() public model: EmailTemplateModel;
    @Output() edit: EventEmitter<EmailTemplateModel> = new EventEmitter(true);

    backup: EmailTemplateModel;
    editing = false;
    disabled = false;

    constructor(
        private templateService: EmailTemplateService,
        private translate: TranslateService) {}

    ngOnInit() {}

    startEditing() {
        this.edit.emit(this.model);
        this.backup = new EmailTemplateModel(this.model);
        this.editing = true;
    }

    save(form) {
        this.disabled = true;
        this.templateService.update(form.value).then(
            (rsp: EmailTemplateModel) => {
                Object.assign(this.model, rsp);
                this.backup = null;
                this.disabled = false;
                this.editing = false;
            },
            (err: ErrorResponse) => {
                console.log(err.message);
                alert(
                    this.translate.instant('EMAIL_TEMPLATE.MESSAGE.ERR_PREFIX', this.model)
                    +
                    this.translate.instant('EMAIL_TEMPLATE.MESSAGE.' + err.token)
                );
            }
        );
        return false;
    }

    cancelUpdate() {
        if (this.editing) {
            Object.assign(this.model, this.backup);
            this.backup = null;
            this.editing = false;
        }
    }
}
