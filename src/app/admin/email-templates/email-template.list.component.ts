import {Component, OnInit, ViewChildren, QueryList} from '@angular/core';

import {EmailTemplateModel} from './email-template.model';
import {EmailTemplateService} from './email-template.service';
import {EmailTemplateComponent} from './email-template.component';

@Component({
    selector: 'app-email-template.list',
    templateUrl: './email-template.list.component.html',
    styleUrls: ['./email-template.list.component.scss']
})
export class EmailTemplateListComponent implements OnInit {
    @ViewChildren(EmailTemplateComponent) children: QueryList<
        EmailTemplateComponent
    >;

    templates: Array<EmailTemplateModel>;
    loading: boolean;
    constructor(private templateService: EmailTemplateService) {}

    ngOnInit() {
        this.loading = true;
        this.templateService
            .list()
            .then((result: Array<EmailTemplateModel>) => {
                this.templates = result;
                this.loading = false;
            });
    }

    onEdit(element: EmailTemplateModel) {
        this.children.forEach((el, i) => {
            if (i !== this.templates.indexOf(element)) {
                el.cancelUpdate();
            }
        });
    }
}
