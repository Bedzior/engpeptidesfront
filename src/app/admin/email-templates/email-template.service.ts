import {EmailTemplateModel} from './email-template.model';

export abstract class EmailTemplateService {
    abstract list(): Promise<Array<EmailTemplateModel>>;
    abstract getSingle(id: number): Promise<EmailTemplateModel>;
    abstract update(item: EmailTemplateModel): Promise<EmailTemplateModel>;
}
