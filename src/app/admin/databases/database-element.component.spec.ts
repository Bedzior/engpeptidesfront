import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {DatabaseElementComponent} from 'app/admin';
import {MockDatabaseServiceProvider} from 'test/services';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FileSizePipe} from 'app/common';
import {DatabaseElementModel} from 'app/admin/databases';
import {FileSingleUploadComponent} from 'app/common';

describe('DatabaseElementComponent', () => {
    let component: DatabaseElementComponent;
    let fixture: ComponentFixture<DatabaseElementComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                ReactiveFormsModule,
                FormsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [
                MockDatabaseServiceProvider,
                FileSingleUploadComponent,
                TranslateService
            ],
            declarations: [DatabaseElementComponent, FileSizePipe],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DatabaseElementComponent);
        component = fixture.componentInstance;
        component.model = new DatabaseElementModel({
            id: 1,
            name: 'abc',
            description: 'Abbdbd'
        });
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
