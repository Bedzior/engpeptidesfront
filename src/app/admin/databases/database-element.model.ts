export class DatabaseElementModel {
    id: number;
    name: string;
    description: string;
    downloading: boolean;
    path: string;
    url: string;
    added: number;
    regexId: string;
    regexName: string;
    token: string;
    size: number;

    constructor(json: string | Object) {
        if (json instanceof Object) {
            Object.assign(this, json);
        } else {
            Object.assign(this, JSON.parse(json));
        }
        return this;
    }
}
