import { Component, HostListener, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { DatabaseElementModel } from './database-element.model';
import { DatabaseService } from './database.service';

@Component({
  selector: 'app-database-list',
  templateUrl: './database-list.component.html',
  styleUrls: ['./database-list.component.scss']
})
export class DatabaseListComponent implements OnInit {

  databases: Array<DatabaseElementModel> = [];
  expandedID: number;
  loading: boolean = false;

  constructor(
      private dbservice: DatabaseService,
      private translate: TranslateService) { }

  ngOnInit() {
    this.loading = true;
    this.dbservice.list().then((rsp) => {
      this.databases = rsp || [];
      this.loading = false;
      window.dispatchEvent(new Event('resize'));
    }).catch((e) => {
      alert(this.translate.instant('DATABASE.MESSAGE.ERR_LISTING'));
      console.error('Error retrieving databases', e);
      this.loading = false;
    });
  }
  /**
   * Creates a new list element for a fresh peptides' database that has to be filled out by the admin.
   */
  createElement() {
    let database = this.databases[this.databases.length - 1];
    if (database && !database.id) {
      // TODO highlight the current form
      return;
    }
    window.dispatchEvent(new Event('resize'));
    this.databases.push(new DatabaseElementModel({
      name: 'New peptides database'
    }));
  }

  @HostListener('window:click')
  expandedChanged(id) {
    window.dispatchEvent(new Event('resize'));
    if (id instanceof Event) {
      this.expandedID = undefined;
    } else if (id) {
      this.expandedID = Number.parseInt(id);
    }
  }
  /**
   * Called when a database element is deleted
   */
  deleted(el: DatabaseElementModel) {
    this.databases.splice(this.databases.findIndex((v) => el.name === v.name), 1);
  }

  cancelAdd() {
    this.databases.splice(this.databases.length - 1, 1);
  }
}
