import {Observable, Subscription} from 'rxjs';
import {
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {ErrorResponse, Expandable, FileSizePipe, inputShrink} from 'app/common';
import {DatabaseElementModel} from './database-element.model';
import {DatabaseService} from './database.service';

const fileSizePipe = new FileSizePipe();

@Component({
    selector: 'app-database-element',
    animations: [inputShrink],
    templateUrl: './database-element.component.html',
    styleUrls: ['./database-element.component.scss']
})
export class DatabaseElementComponent implements OnInit, OnDestroy, Expandable {
    private subscription: Subscription;

    disabled: boolean = false;
    edit: boolean = false;
    add: boolean = false;

    private temp: DatabaseElementModel;
    @Input() model: DatabaseElementModel;

    @Input() expanded: boolean;
    @Output() expandedChange: EventEmitter<boolean> = new EventEmitter(true);

    @Output() cancelAdd: EventEmitter<any> = new EventEmitter(true);
    @Output()
    deleted: EventEmitter<DatabaseElementModel> = new EventEmitter(true);

    progress: number = -1;
    total: number = -1;

    constructor(
        private dbService: DatabaseService,
        private translate: TranslateService
    ) {}

    ngOnInit() {
        if (!this.model.id) {
            this.add = true;
        }
        if (this.model.downloading) {
            this.dbService.subscribe(this.model.name).subscribe((object) => {
                if (object.hasOwnProperty('progress')) {
                    this.progress = object['progress'];
                    this.total = object['total'];
                } else {
                    this.model = new DatabaseElementModel(object);
                }
                return object;
            });
        }
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        this.dbService.unsubscribe();
    }

    focus() {
        if (!this.expand) {
            return false;
        }
    }

    cancel() {
        if (this.add) {
            this.cancelAdd.emit();
        }
        this.edit = false;
        if (this.model.downloading) {
            this.dbService.cancel(this.model.name);
        }
    }
    startEditing() {
        this.edit = true;
        this.temp = new DatabaseElementModel(this.model);
    }
    submit(event) {
        if (this.edit) {
            this.dbService.update(this.model).then(
                (model) => {
                    this.model = model;
                    alert(
                        this.translate.instant(
                            'DATABASE.MESSAGE.UPDATE_SUCCESS',
                            this.model
                        )
                    );
                    this.edit = false;
                    this.temp = undefined;
                },
                (e: ErrorResponse) => {
                    console.log('Could not update database ' + this.model.name);
                    alert(
                        this.translate.instant('DATABASE.MESSAGE.ERR_PREFIX') +
                            this.translate.instant(
                                'DATABASE.MESSAGE.' + e.token
                            )
                    );
                    this.edit = false;
                    this.model = Object.assign(this.model, this.temp);
                    this.temp = undefined;
                }
            );
        } else if (this.add) {
            let obs = this.dbService.create(this.model);
            if (obs['subscribe']) {
                this.model.downloading = true;
                let v = (<Observable<{}>>obs).subscribe(
                    (object) => {
                        if (object.hasOwnProperty('progress')) {
                            this.progress = object['progress'];
                            this.total = object['total'];
                        } else {
                            Object.assign(this.model, object);
                        }
                        return object;
                    },
                    (rsp: ErrorResponse) => {
                        this.progress = -1;
                        this.total = -1;
                        console.log(rsp.message);
                        alert(
                            this.translate.instant(
                                'DATABASE.MESSAGE.ERR_DL_PREFIX'
                            ) +
                                this.translate.instant(
                                    'DATABASE.MESSAGE.' + rsp.token
                                )
                        );
                        return rsp;
                    },
                    () => {
                        this.model.downloading = false;
                        this.add = false;
                        v.unsubscribe();
                    }
                );
            } else {
                (<Promise<DatabaseElementModel>>obs)
                    .then((db) => {
                        this.add = false;
                        this.model = new DatabaseElementModel(db);
                    })
                    .catch((err: ErrorResponse) => {
                        console.error(err.message);
                        alert(
                            this.translate.instant(
                                'DATABASE.MESSAGE.ERR_CREATE_PREFIX',
                                this.model
                            ) +
                                this.translate.instant(
                                    'DATABASE.MESSAGE.' + err.token
                                )
                        );
                    });
            }
        }
        window.dispatchEvent(new Event('resize'));
    }
    onURLPaste(event: ClipboardEvent): boolean {
        let url = event.clipboardData.getData('text/plain');
        if (url) {
            const lastDelim = url.lastIndexOf('/');
            if (lastDelim > 0) {
                this.model.name = url.substring(
                    lastDelim + 1,
                    url.indexOf('.', lastDelim)
                );
                return true;
            }
        }
        return false;
    }
    delete() {
        if (
            confirm(
                this.translate.instant('DATABASE.CONFIRM.DELETE', this.model)
            )
        ) {
            this.dbService.remove(this.model.id).then(
                () => {
                    alert(
                        this.translate.instant(
                            'DATABASE.MESSAGE.DELETE_SUCCESS',
                            this.model
                        )
                    );
                    this.deleted.emit(this.model);
                },
                (err: ErrorResponse) => {
                    alert(
                        this.translate.instant(
                            'DATABASE.MESSAGE.ERR_DELETE_PREFIX',
                            this.model
                        ) +
                            this.translate.instant(
                                'DATABASE.MESSAGE.' + err.token
                            )
                    );
                }
            );
        }
    }
    getReadableSize(size: number): String {
        return fileSizePipe.transform(size);
    }
    expand(): boolean {
        if (!this.expanded) {
            this.expanded = true;
            this.expandedChange.emit(this.expanded);
            return true;
        }
        return false;
    }
    collapse(): boolean {
        if (this.expanded) {
            this.expanded = false;
            this.expandedChange.emit(this.expanded);
            return true;
        }
        return false;
    }
}
