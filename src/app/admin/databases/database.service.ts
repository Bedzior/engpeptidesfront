import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/observeOn';
import {Observable} from 'rxjs/Observable';

import {Injectable, Injector, ClassProvider} from '@angular/core';

import {IAppConfig, APIService, SocketService} from 'app/common';
import {DatabaseElementModel} from './database-element.model';

const endpoint = '/secured/dbs';
export abstract class DatabaseService {
    abstract list(): Promise<Array<DatabaseElementModel>>;
    abstract create(
        form: DatabaseElementModel
    ): Promise<DatabaseElementModel> | Observable<{}>;
    abstract unsubscribe();
    abstract subscribe(name: string): Observable<{}>;
    abstract update(form: DatabaseElementModel): Promise<DatabaseElementModel>;
    abstract cancel(name: string);
    abstract remove(id: number): Promise<number>;
}
@Injectable()
export class DatabaseServiceImpl extends APIService implements DatabaseService {
    protected subscribed: boolean;

    constructor(
        protected conf: IAppConfig,
        protected socketService: SocketService,
        protected injector: Injector
    ) {
        super(conf, injector);
        this.socketService = injector.get(SocketService);
    }

    /**
     * Retrieves a full list of databases
     */
    public list(): Promise<Array<DatabaseElementModel>> {
        let promise = this.get(endpoint);
        promise.then(() => {
            console.log('Retrieved database list');
        });
        return promise;
    }
    public create(
        form: DatabaseElementModel
    ): Promise<DatabaseElementModel> | Observable<{}> {
        if (form.url) {
            // we expect a download
            let observable: Observable<{}> = new Observable(
                ((observer) => {
                    this.post(endpoint, form)
                        .then(
                            ((rsp: DatabaseElementModel) => {
                                this.subscribeToTopic(form.name, observer);
                            }).bind(this)
                        )
                        .catch((error) => {
                            try {
                                observer.error(error);
                            } catch (e) {
                                observer.error({message: error.statusText});
                            }
                            observer.complete();
                        });
                }).bind(this)
            );
            return observable;
        } else {
            // just wait for the db to be created
            return this.post(endpoint, form).then((rsp) => rsp);
        }
    }
    public unsubscribe() {
        this.subscribed = false;
        this.socketService.unsubscribe();
    }
    public subscribe(name: string): Observable<{}> {
        return new Observable((observer) =>
            this.subscribeToTopic(name, observer)
        );
    }
    public update(form: DatabaseElementModel): Promise<DatabaseElementModel> {
        return this.put(endpoint + '/' + form.id, form);
    }
    public cancel(name: string) {
        this.socketService.publish('/app/db_upload/' + name, {
            command: 'cancel'
        });
    }
    public remove(id: number): Promise<number> {
        return this.delete(endpoint, id).then(() => {
            console.info('Successfully deleted database');
            return id;
        });
    }
    private subscribeToTopic(name: string, observer: any): any {
        if (!this.subscribed) {
            this.subscribed = true;
        }
        let sub = this.socketService.subscribe('/topic/db_upload/' + name);
        sub.subscribe((json) => {
            if (!this.subscribed) {
                observer.unsubscribe();
            } else {
                observer.next(json);
                if (!json.hasOwnProperty('progress')) {
                    observer.complete();
                }
            }
        });
    }
}

export const DatabaseServiceProvider: ClassProvider = {
    provide: DatabaseService,
    useClass: DatabaseServiceImpl
};
