import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {
    TranslateService,
    TranslateModule,
    TranslateLoader
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {DatabaseListComponent} from './database-list.component';
import {DatabaseElementComponent} from './database-element.component';
import {FileSingleUploadComponent, FileSizePipe} from 'app/common';
import {
    MockAuthorizationServiceProvider,
    MockAppConfigProvider,
    MockDatabaseServiceProvider
} from 'test/shared';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('DatabaseListComponent', () => {
    let component: DatabaseListComponent;
    let fixture: ComponentFixture<DatabaseListComponent>;

    beforeEach(async(() => {
        TestBed.overrideComponent(FileSingleUploadComponent, {set: {}});
        TestBed.configureTestingModule({
            providers: [
                MockAppConfigProvider,
                MockAuthorizationServiceProvider,
                MockDatabaseServiceProvider,
                TranslateService
            ],
            imports: [
                MatProgressSpinnerModule,
                ReactiveFormsModule,
                FormsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [
                FileSizePipe,
                DatabaseListComponent,
                DatabaseElementComponent
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DatabaseListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
