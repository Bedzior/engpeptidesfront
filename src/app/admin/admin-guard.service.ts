import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {
    CanActivate,
    CanActivateChild,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from '@angular/router';

import {AuthorizationService} from 'app/common';

@Injectable()
export class AdminGuard implements CanActivate, CanActivateChild {
    constructor(private auth: AuthorizationService) {}
    canActivateChild(
        childRoute: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean> {
        return this.canActivate(childRoute, state);
    }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean> {
        if (this.auth.hasRole('admin')) {
            return true;
        }
        return false;
    }
}
