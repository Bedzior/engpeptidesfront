export interface ServerStatus {
    scanCount: number;
    waitingScanCount: number;
}
