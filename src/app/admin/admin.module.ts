import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {BrowserModule, Title} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PRIMARY_OUTLET, RouterModule, Routes} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {StompRService} from '@stomp/ng2-stompjs';
import {CookieService} from 'angular2-cookie';
import {FileSizePipe, LoggedInGuard, SocketServiceProvider} from 'app/common';
import {AppCommonModule} from 'app/common/common.module';
import {NgxEditorModule} from 'ngx-editor';

import {AdminGuard} from './admin-guard.service';
import {AdminComponent} from './admin.component';
import {AdminServiceProvider} from './admin.service';
import {AdminConfigurationComponent} from './configuration/admin-configuration.component';
import {AdminConfigurationServiceProvider} from './configuration/admin-configuration.service';
import {ConfigurationControlComponent} from './configuration/configuration-control.component';
import {
    DatabaseElementComponent,
    DatabaseListComponent,
    DatabaseServiceProvider
} from './databases';
import {EmailTemplateComponent} from './email-templates/email-template.component';
import {EmailTemplateListComponent} from './email-templates/email-template.list.component';
import {EmailTemplateServiceProvider} from './email-templates/email-template.service.provider';
import {UserEntityComponent, UserListComponent} from './users';
import {UsersServiceProvider} from './users/users.service.provider';

const routes: Routes = [
    {
        path: 'admin',
        component: AdminComponent,
        data: {
            title: 'ADMIN.MAIN'
        },
        canActivate: [LoggedInGuard, AdminGuard],
        canActivateChild: [LoggedInGuard, AdminGuard],
        children: [
            {
                path: '',
                pathMatch: 'full',
                data: {
                    title: 'ADMIN.MAIN'
                },
                redirectTo: '/admin/configuration'
            },
            {
                path: 'databases',
                component: DatabaseListComponent,
                outlet: PRIMARY_OUTLET,
                data: {
                    title: 'ADMIN.DATABASE_LIST'
                },
                children: [
                    {
                        path: ':id',
                        component: DatabaseElementComponent,
                        outlet: 'element',
                        data: {
                            title: 'ADMIN.DATABASE_ITEM'
                        }
                    }
                ]
            },
            {
                path: 'users',
                component: UserListComponent,
                outlet: PRIMARY_OUTLET,
                data: {
                    title: 'ADMIN.USERS'
                },
                children: [
                    {
                        path: ':id',
                        component: UserEntityComponent
                    }
                ]
            },
            {
                path: 'templates',
                component: EmailTemplateListComponent,
                outlet: PRIMARY_OUTLET,
                data: {
                    title: 'ADMIN.TEMPLATES'
                },
                children: [
                    {
                        path: ':id',
                        component: EmailTemplateComponent
                    }
                ]
            },
            {
                path: 'configuration',
                component: AdminConfigurationComponent,
                outlet: PRIMARY_OUTLET,
                data: {
                    title: 'ADMIN.CONFIG'
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        TranslateModule.forChild({}),
        BrowserAnimationsModule,
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        HttpClientModule,
        RouterModule.forRoot(routes),
        MatProgressSpinnerModule,
        AppCommonModule,
        NgxEditorModule
    ],
    bootstrap: [AdminComponent],
    declarations: [
        AdminComponent,
        DatabaseListComponent,
        DatabaseElementComponent,
        UserListComponent,
        UserEntityComponent,
        FileSizePipe,
        EmailTemplateComponent,
        EmailTemplateListComponent,
        AdminConfigurationComponent,
        ConfigurationControlComponent
    ],
    providers: [
        Title,
        StompRService,
        CookieService,
        AdminGuard,
        LoggedInGuard,
        SocketServiceProvider,
        DatabaseServiceProvider,
        UsersServiceProvider,
        EmailTemplateServiceProvider,
        AdminConfigurationServiceProvider,
        AdminServiceProvider
    ]
})
export class AdminModule {}
