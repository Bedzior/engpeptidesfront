import {
    ControlPanelItem,
    SeparatorItem
} from '../../common/control-panel/control-panel-item';

export const elements: Array<ControlPanelItem | SeparatorItem> = [{
  id: 'configuration',
  route: '/admin/configuration',
  icon: 'fa-sliders'
}, {
  id: 'users',
  route: '/admin/users',
  icon: 'fa-users'
}, {
  id: 'databases',
  route: '/admin/databases',
  icon: 'fa-database'
}, {
  id: 'templates',
  route: '/admin/templates',
  icon: 'fa-envelope'
}, new SeparatorItem(), {
  id: 'logout',
  route: '/logout',
  icon: 'fa-sign-out'
}];
