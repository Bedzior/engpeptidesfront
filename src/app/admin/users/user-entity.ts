export interface UserEntity {
    id: number;
    login: string;
    email: string;
    activated: string|boolean;
    admin: boolean;
}
