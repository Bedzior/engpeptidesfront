import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

import {AuthorizationService} from 'app/common';
import {UserEntity} from './user-entity';
import {UsersService} from './users.service';

@Component({
    selector: '[app-user-entity]',
    templateUrl: './user-entity.component.html',
    styleUrls: ['./user-entity.component.scss']
})
export class UserEntityComponent implements OnInit {
    @Input() model: UserEntity;
    @Output()
    onRemoved: EventEmitter<UserEntity> = new EventEmitter<UserEntity>();
    enabled: boolean = true;
    constructor(
        private translate: TranslateService,
        private authService: AuthorizationService,
        private usersService: UsersService
    ) {}

    ngOnInit() {}

    delete(model: UserEntity) {
        if (
            confirm(
                this.translate.instant(
                    'USER.ELEMENT.ACTIONS.MESSAGES.CONFIRM_DELETE',
                    {user: model.login}
                )
            )
        ) {
            this.enabled = false;
            this.usersService
                .remove(model)
                .then((v) => {
                    this.onRemoved.emit(model);
                    this.enabled = true;
                })
                .catch(() => {
                    alert(
                        this.translate.instant(
                            'USER.ELEMENT.ACTIONS.MESSAGES.ERROR_DELETE',
                            {user: model.login}
                        )
                    );
                    this.enabled = true;
                });
        }
    }

    setRole(model: UserEntity, admin: boolean | string) {
        if (typeof admin === 'string') {
            admin = admin === 'true';
        }
        if ((model.admin && !admin) || (!model.admin && admin)) {
            model.admin = admin;
            this.enabled = false;
            this.usersService
                .update(model)
                .then(
                    () =>
                        console.log('Successfully updated user ' + model.login),
                    () => {
                        model.admin = !admin;
                        alert(
                            this.translate.instant(
                                'USER.ELEMENT.ACTIONS.MESSAGES.ERROR_UPDATE',
                                {user: model.login}
                            )
                        );
                    }
                )
                .then(() => {
                    this.enabled = true;
                });
        }
    }

    activate(model: UserEntity) {
        if (!model.activated) {
            model.activated = true;
            this.usersService
                .update(model)
                .then(
                    () =>
                        alert(
                            this.translate.instant('USER.ELEMENT.ACTIONS.MESSAGES.ACTIVATED',
                            {user: model.login})
                        ),
                    () => {
                        model.activated = false;
                        alert(
                            this.translate.instant(
                                'USER.ELEMENT.ACTIONS.MESSAGES.ERROR_ACTIVATE',
                                {user: model.login}
                            )
                        );
                    }

                );
        }
    }

    isCurrentUser(): boolean {
        return this.authService.getLogin() === this.model.login;
    }
}
