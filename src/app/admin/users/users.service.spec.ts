import {TestBed, inject} from '@angular/core/testing';

import {UsersService} from 'app/admin';
import {MockAppConfigProvider} from 'test/app.config';

describe('UsersService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [MockAppConfigProvider, UsersService]
        });
    });

    it('should be created', inject([UsersService], (service: UsersService) => {
        expect(service).toBeTruthy();
    }));
});
