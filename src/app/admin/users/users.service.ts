import {UserEntity} from './user-entity';

export abstract class UsersService {
    abstract listAll(): Promise<UserEntity[]>;
    abstract update(user: UserEntity): Promise<UserEntity>;
    abstract remove(user: UserEntity): Promise<UserEntity>;
}
