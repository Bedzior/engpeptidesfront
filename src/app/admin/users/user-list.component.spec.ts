import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {MatProgressSpinnerModule} from '@angular/material';
import {CookieService} from 'angular2-cookie';
import {
    TranslateService,
    TranslateModule,
    TranslateLoader
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {UserListComponent} from './user-list.component';
import {MockUsersServiceProvider} from 'test/services/users.service';
import {MockAuthorizationServiceProvider} from 'test/services/authorization.service';

describe('UserListComponent', () => {
    let component: UserListComponent;
    let fixture: ComponentFixture<UserListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            providers: [
                CookieService,
                MockAuthorizationServiceProvider,
                MockUsersServiceProvider,
                TranslateService
            ],
            imports: [
                MatProgressSpinnerModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            declarations: [UserListComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
