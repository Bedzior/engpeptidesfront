import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';

import {UserEntityComponent} from './user-entity.component';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {
    MockAuthorizationServiceProvider,
    MockUsersServiceProvider
} from 'test/services';

describe('UserEntityComponent', () => {
    let component: UserEntityComponent;
    let fixture: ComponentFixture<UserEntityComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [
                MockAuthorizationServiceProvider,
                MockUsersServiceProvider,
                TranslateService
            ],
            declarations: [UserEntityComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UserEntityComponent);
        component = fixture.componentInstance;
        component.model = {
            id: 1,
            login: 'abc',
            email: 'dot@example.com',
            activated: 'true',
            admin: false
        };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
