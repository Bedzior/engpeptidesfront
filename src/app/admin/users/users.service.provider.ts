import {ClassProvider} from '@angular/core';

import {UsersService} from './users.service';
import {UsersServiceImpl} from './private/users.service';

export const UsersServiceProvider: ClassProvider = {
    provide: UsersService,
    useClass: UsersServiceImpl
};
