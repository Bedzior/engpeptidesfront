import {Injectable, Injector} from '@angular/core';

import {IAppConfig, APIService} from 'app/common';
import {UserEntity} from '../user-entity';
import {UsersService} from '../users.service';

const endpoint = '/secured/admin/users';

@Injectable()
export class UsersServiceImpl extends APIService implements UsersService {
    constructor(protected conf: IAppConfig, injector: Injector) {
        super(conf, injector);
    }

    public listAll(): Promise<[UserEntity]> {
        return this.get(endpoint).catch((reason) => {
            console.error("Couldn't retrieve users' list", reason);
            throw reason;
        });
    }
    public update(user: UserEntity): Promise<UserEntity> {
        return this.put(endpoint + '/' + user.id, user).catch((reason) => {
            console.error("Couldn't update user ", user.login);
            throw reason;
        });
    }
    public remove(user: UserEntity): Promise<UserEntity> {
        return this.delete(endpoint, user.id).catch((reason) => {
            console.error("Couldn't delete user ", user.login);
            throw reason;
        });
    }
}
