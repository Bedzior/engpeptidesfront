import { Component, OnInit } from '@angular/core';

import { UsersService } from './users.service';
import { UserEntity } from './user-entity';
import { AuthorizationService } from 'app/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users: Array<UserEntity>;

  loading: boolean;

  constructor(
    private authService: AuthorizationService,
    private usersService: UsersService,
    private translate: TranslateService
  ) {
    this.loading = true;
  }

  ngOnInit() {
    this.usersService.listAll().then(list => {
      this.users = list;
      this.loading = false;
      window.dispatchEvent(new Event('resize'));
    }).catch(() => {
      alert(this.translate.instant('USER.LIST.MESSAGE.ERR_LISTING'));
    });
  }

  isCurrentUser(model: UserEntity): boolean {
    return this.authService.getLogin() === model.login;
  }

  onModelDelete(model: UserEntity): void {
    let index = this.users.findIndex((user) => { return user === model; });
    this.users.splice(index, 1);
  }
}
