import {ClassProvider, Injectable, Injector} from '@angular/core';

import {APIService, IAppConfig} from 'app/common';
import {ServerStatus} from './server-status';

export abstract class AdminService {
    abstract getStatus(): Promise<ServerStatus>;
}

@Injectable()
export class AdminServiceImpl extends APIService implements AdminService {
    constructor(protected conf: IAppConfig, protected injector: Injector) {
        super(conf, injector);
    }

    getStatus(): Promise<ServerStatus> {
        return this.get('/secured/admin/status');
    }
}

export const AdminServiceProvider: ClassProvider = {
    provide: AdminService,
    useClass: AdminServiceImpl
};
