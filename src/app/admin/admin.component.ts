import {Component, OnInit, OnDestroy} from '@angular/core';

import {AuthorizationService, SocketService} from 'app/common';
import {AdminService} from './admin.service';
import {ServerStatus} from './server-status';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit, OnDestroy {
    interval: NodeJS.Timer;

    loading: boolean = true;
    status: ServerStatus;

    constructor(
        private auth: AuthorizationService,
        private socketService: SocketService,
        private adminService: AdminService
    ) {}

    ngOnInit(): void {
        this.reload();
        this.interval = setInterval(() => this.reload(), 5 * 1000);
    }

    ngOnDestroy(): void {
        clearInterval(this.interval);
    }

    reload(): void {
        this.loading = true;
        this.adminService.getStatus().then(
            (status) => {
                this.status = status;
                this.loading = false;
            },
            (err: Error) => {
                this.loading = false;
                console.error('Error retrieving server status: ', err);
            }
        );
    }
}
