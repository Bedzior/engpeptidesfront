import {Component, OnInit} from '@angular/core';

/**
 * Non-functional component which use is solely to replace
 * {@link AboutComponent}'s content in the router's outlet
 */
@Component({
    selector: 'app-about-placeholder',
    template: `
        <div style="height:420px"></div>
    `,
    styles: []
})
export class AboutPlaceholderComponent implements OnInit {
    constructor() {}

    ngOnInit() {}
}
