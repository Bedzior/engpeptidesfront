import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';

import {AppCommonComponent} from 'app/common';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';

describe('AppCommonComponent', () => {
    let component: AppCommonComponent;
    let fixture: ComponentFixture<AppCommonComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AppCommonComponent],
            providers: [TranslateService],
            imports: [
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppCommonComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
