import {inject, TestBed} from '@angular/core/testing';
import {StompRService} from '@stomp/ng2-stompjs';
import {CookieService} from 'angular2-cookie';

import {SocketService} from 'app/common';
import {
    MockAuthorizationServiceProvider,
    MockSocketServiceProvider
} from 'test/services';

describe('SocketService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                MockSocketServiceProvider,
                MockAuthorizationServiceProvider,
                StompRService,
                CookieService
            ]
        });
    });

    it('should be created', inject(
        [SocketService],
        (service: SocketService) => {
            expect(service).toBeTruthy();
        }
    ));
});
