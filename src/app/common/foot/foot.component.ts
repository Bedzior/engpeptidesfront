import {Component} from '@angular/core';

@Component({
    selector: 'app-foot',
    template: `
    <footer #footer [style.bottom.px]="bottom">
        {{ 'FOOT.ABOUT_1' | translate }}
        <span class="author">{{author}}</span>, {{year}}
        (<a href=\"{{repoURL}}\" translate>{{ 'FOOT.REPO' | translate }}</a>)
        {{ 'FOOT.ABOUT_2' | translate }}
        <a [routerLink]="['/about']" translate>FOOT.ABOUT_3</a>
        <br/>
        <a href="{{iconsURL}}" translate>FOOT.ICONS</a>
    </footer>`,
    styleUrls: ['./foot.component.scss']
})
export class FootComponent {
    author: string = 'Rafał Będźkowski';
    year: Number = 2018;
    bottom: number;
    repoURL: URL = new URL('https://bitbucket.org/Bedzior/peptides');
    iconsURL: URL = new URL('https://icons8.com');

    constructor() {}
}
