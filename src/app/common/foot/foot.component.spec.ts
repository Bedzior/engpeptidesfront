import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateModule,
    TranslateLoader,
    TranslateService
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {FootComponent} from './foot.component';

describe('FootComponent', () => {
    let component: FootComponent;
    let fixture: ComponentFixture<FootComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [TranslateService],
            declarations: [FootComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FootComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it("should contain a link to the site images' source", () => {
        expect(fixture.nativeElement.querySelector('a[href*="icons8.com"]'));
    });
});
