export abstract class IAppConfig {
    apiEndpoint: string;
    socketEndpoint: string;
    uploadEndpoint: string;
}
