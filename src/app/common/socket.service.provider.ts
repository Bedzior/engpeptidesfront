import {ClassProvider} from '@angular/core';
import {SocketService} from './socket.service';
import {SocketServiceImpl} from './private/socket.service';

export const SocketServiceProvider: ClassProvider = {
    provide: SocketService,
    useClass: SocketServiceImpl
};
