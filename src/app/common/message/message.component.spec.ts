/* tslint:disable:no-unused-variable */
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateModule,
    TranslateLoader,
    TranslateService
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {MessageComponent} from './message.component';

describe('MessageComponent', () => {
    let component: MessageComponent;
    let fixture: ComponentFixture<MessageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MessageComponent],
            providers: [TranslateService],
            imports: [
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MessageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain a simple message', () => {
        let msg = fixture.debugElement.nativeElement.querySelector('p');
        expect(msg).toBeTruthy();
    });
});
