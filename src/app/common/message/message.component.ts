import {Subscription} from 'rxjs';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit, OnDestroy {
    public link: any;

    private subscription: Subscription;

    public type: string;

    constructor(
        private route: ActivatedRoute,
        public translate: TranslateService
    ) {}

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.link = null;
            this.type = params['type'];
            if (this.type === 'logout') {
                this.link = {route: '/login', name: 'LOGIN_PAGE'};
            }
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
