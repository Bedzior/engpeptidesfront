export class UserModel {
    private id: number;
    public login: string;
    public email: string;
    public activated: boolean;
    public roles: Array<string>;

    public constructor(json?: string) {
        if (json) {
            Object.assign(this, JSON.parse(json));
        }
        return this;
    }

    public getEmail(): string {
        return this.email;
    }
    public getLogin(): string {
        return this.login;
    }
    public isActivated(): boolean {
        return this.activated;
    }
    public getRoles(): Array<string> {
        let roles = [];
        for (let role of this.roles) {
            roles.push(role.toLowerCase());
        }
        return roles;
    }
}
