import {Subscription} from 'rxjs';
import {Component, Input, OnDestroy} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

import {AuthorizationService} from '../auth/authorization.service';
import {ControlPanelItem, SeparatorItem} from './control-panel-item';

@Component({
    selector: 'app-control-panel',
    templateUrl: './control-panel.component.html',
    styleUrls: ['./control-panel.component.scss']
})
export class ControlPanelComponent implements OnDestroy {
    @Input()
    elements: Array<ControlPanelItem | SeparatorItem>;

    private active: string;

    private subscription: Subscription;

    constructor(public auth: AuthorizationService, protected router: Router) {
        this.subscription = router.events
            .filter((event) => {
                return event instanceof NavigationEnd;
            })
            .subscribe((event: NavigationEnd) => {
                this.active = event.urlAfterRedirects;
                console.log('Navigated to', this.router.url);
            });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
