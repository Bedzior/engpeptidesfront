export class ControlPanelItem {
    public id: string;
    public route?: string;
    public children?: Array<ControlPanelItem>;
    public icon?: string;
    public separator? = false;
}
export class SeparatorItem {
    public separator: boolean;
    constructor() {
        this.separator = true;
    }
}
