import {CommonModule} from '@angular/common';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import {RouterModule, Routes} from '@angular/router';
import {Title, BrowserModule} from '@angular/platform-browser';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {FileUploadModule} from 'ng2-file-upload';
import {TranslateModule} from '@ngx-translate/core';

import {AppConfigProvider} from './app.config.provider';
import {AppCommonComponent} from './common.component';
import {FootComponent} from './foot/foot.component';
import {MessageComponent} from './message/message.component';
import {ReactiveInputComponent} from './forms/reactive-input/reactive-input.component';
import {FileSingleUploadComponent} from './forms/file-upload/file-single-upload.component';
import {FileSeveralUploadComponent} from './forms/file-upload/file-several-upload.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {ControlPanelComponent} from './control-panel/control-panel.component';
import {AuthorizationServiceProvider} from './auth/authorization.service.provider';
import {IdentityServiceProvider} from './identity.service.provider';

export const commonRoutes: Routes = [
    {
        path: 'msg',
        component: MessageComponent,
        data: {
            title: 'COMMON.MESSAGE'
        }
    },
    {
        path: '404',
        component: NotFoundComponent,
        data: {
            title: '🤖 bzzz <404> bzzz 🤖'
        }
    }
];

@NgModule({
    declarations: [
        AppCommonComponent,
        FootComponent,
        NotFoundComponent,
        MessageComponent,
        FileSeveralUploadComponent,
        FileSingleUploadComponent,
        ReactiveInputComponent,
        ControlPanelComponent
    ],
    imports: [
        TranslateModule.forChild(),
        BrowserModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        FileUploadModule,
        RouterModule.forChild(commonRoutes),
        MatProgressSpinnerModule
    ],
    exports: [
        FootComponent,
        NotFoundComponent,
        MessageComponent,
        AppCommonComponent,
        FileSeveralUploadComponent,
        FileSingleUploadComponent,
        ReactiveInputComponent,
        ControlPanelComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [
        AppCommonComponent,
        ReactiveInputComponent,
        FileSeveralUploadComponent,
        FileSingleUploadComponent
    ],
    providers: [
        Title,
        AuthorizationServiceProvider,
        AppConfigProvider,
        IdentityServiceProvider
    ]
})
export class AppCommonModule {}
