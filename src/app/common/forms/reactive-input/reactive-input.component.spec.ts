import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';

import {ReactiveInputComponent} from './reactive-input.component';
import {MockAuthorizationServiceProvider} from 'test/shared';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateService} from '@ngx-translate/core';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';

describe('ReactiveInputComponent', () => {
    let component: ReactiveInputComponent;
    let fixture: ComponentFixture<ReactiveInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                BrowserAnimationsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [MockAuthorizationServiceProvider, TranslateService],
            declarations: [ReactiveInputComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ReactiveInputComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
