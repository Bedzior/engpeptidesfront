import {ViewChild, Input, Component, OnInit, forwardRef} from '@angular/core';
import {AbstractControl, ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

import {placeholderSwitch, tooltipSwitch} from '../../animations';

@Component({
    selector: 'app-reactive-input',
    templateUrl: './reactive-input.component.html',
    animations: [placeholderSwitch, tooltipSwitch],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ReactiveInputComponent),
            multi: true
        }
    ],
    styleUrls: ['./reactive-input.component.scss']
})
export class ReactiveInputComponent implements OnInit, ControlValueAccessor {
    onChange: any;

    @Input() source: Array<any>;
    @Input() formControl: AbstractControl;
    @Input() inputType: string = 'text';
    @Input() name: string;
    @Input() step: number;
    @Input() placeholder: string;
    @Input() autocomplete: boolean;
    @Input() autofocus: boolean;
    @Input() readOnly: boolean;
    @Input() tooltip: string;
    @Input() id: string;
    @Input() value: any;

    @ViewChild('input') private inputEl: HTMLInputElement;
    @ViewChild('select') private selectEl: HTMLSelectElement;
    @ViewChild('tooltip') private tooltipEl;

    toggle: boolean = false;

    constructor(public translate: TranslateService) {}

    public isSelect(): boolean {
        return ['select', 'joint-select'].indexOf(this.inputType) >= 0;
    }

    public isFile(): boolean {
        return this.inputType === 'file' || this.inputType === 'several-files';
    }

    public isArray(): boolean {
        return this.inputType === 'several-files';
    }

    public isInput(): boolean {
        return ['password', 'text', 'number'].indexOf(this.inputType) >= 0;
    }

    public toggleTooltip() {
        this.toggle = !this.toggle;
        let state = this.toggle;
        if (this.toggle) {
            setTimeout(() => {
                if (this.toggle === state) {
                    this.toggleTooltip();
                }
            }, 5000);
        }
    }

    ngOnInit() {}

    writeValue(obj: any): void {
        // this.inputEl.value = obj;
        if (this.onChange) {
            this.onChange(obj);
        }
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
    }

    setDisabledState?(isDisabled: boolean): void {
    }
}
