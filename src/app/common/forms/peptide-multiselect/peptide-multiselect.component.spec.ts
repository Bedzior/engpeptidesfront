import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    FormControl,
    FormsModule,
    ReactiveFormsModule,
    FormGroup
} from '@angular/forms';
import {
    TranslateModule,
    TranslateLoader,
    TranslateService
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {PeptideMultiselectComponent} from './peptide-multiselect.component';

describe('PeptideMultiselectComponent', () => {
    let component: PeptideMultiselectComponent;
    let fixture: ComponentFixture<PeptideMultiselectComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                FormsModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [TranslateService],
            declarations: [PeptideMultiselectComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PeptideMultiselectComponent);
        component = fixture.componentInstance;
        component.group = new FormGroup({
            varModifications: new FormControl(),
            fixModifications: new FormControl()
        });
        component.source = [{name: 'a'}, {name: 'b'}, {name: 'c'}, {name: 'd'}];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
