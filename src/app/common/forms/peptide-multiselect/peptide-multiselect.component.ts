import {Component, Input, ViewChild} from '@angular/core';
import {FormGroup} from '@angular/forms';

import {placeholderSwitch} from 'app/common/animations';

@Component({
    selector: 'app-peptide-multiselect',
    animations: [placeholderSwitch],
    templateUrl: './peptide-multiselect.component.html',
    styleUrls: ['./peptide-multiselect.component.scss']
})
export class PeptideMultiselectComponent {
    @Input()
    group: FormGroup;
    @Input()
    source: Array<any>;

    loading: boolean = false;

    showAdvanced = false; // by default, hide the less common post-translational modifications

    @ViewChild('souceSelect')
    private sourceSelect: HTMLSelectElement;

    constructor() {}
    onChange(e) {
        // this.loading = true;
        // setTimeout(() => {
        //     this.loading = false;
        // }, 0);
    }
}
