import {AbstractControl, FormControl, ValidatorFn} from '@angular/forms';

export class MyValidators {
    private static EMAIL_PATTERN: RegExp = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    private static PASSWORD_PATTERN: RegExp = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/;

    private static LIST_PATTERN = /^((\d+),[ ]?)*\d+$/;

    private static properPassValidator: ValidatorFn;

    private static emailValidator: ValidatorFn;

    private static listOfNumbersValidator: ValidatorFn;

    /**
     * In-place validation for regex password check; <b>at least</b>:<ul>
     *  <li>8 characters long</li>
     *  <li>1 digit</li>
     *  <li>1 letter</li>
     *  <li>1 special</li>
     * </ul>
     */
    static properPassword(c: AbstractControl): {[key: string]: JSON} {
        if (MyValidators.properPassValidator == null) {
            MyValidators.properPassValidator = (control: FormControl) => {
                return MyValidators.PASSWORD_PATTERN.test(control.value)
                    ? null
                    : {properpassword: {valid: false}};
            };
        }
        return MyValidators.properPassValidator(c);
    }
    /**
     * Instantiates a @link{ValidatorFn}, which verifies value equality with another control, specified by @argument controlName
     */
    static equalTo(controlName: string): ValidatorFn {
        return (c: AbstractControl): {[key: string]: any} => {
            let other = c.root.get(controlName);
            return other && c.value === other.value
                ? null
                : {equalto: {valid: false}};
        };
    }

    static email(c: AbstractControl): {[key: string]: any} {
        if (MyValidators.emailValidator == null) {
            MyValidators.emailValidator = (control: FormControl) => {
                let valid: boolean = MyValidators.EMAIL_PATTERN.test(
                    control.value
                );
                return valid ? null : {email: {valid: false}};
            };
        }
        return MyValidators.emailValidator(c);
    }

    static listOfNumbers(c: AbstractControl): {[key: string]: any} {
        if (!MyValidators.listOfNumbersValidator) {
            MyValidators.listOfNumbersValidator = (
                control: AbstractControl
            ): {[key: string]: any} => {
                let list = c.value;
                let valid = MyValidators.LIST_PATTERN.test(list);
                return valid ? null : {listOfNumbers: {valid: false}};
            };
        }
        return MyValidators.listOfNumbersValidator(c);
    }
}
