import {Component, OnInit, Input, ViewChild, forwardRef} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

import {shortNameSort} from 'app/common/sorters';
import {placeholderSwitch, tooltipSwitch} from 'app/common/animations';

@Component({
    selector: 'app-peptide-select',
    animations: [placeholderSwitch, tooltipSwitch],
    templateUrl: './peptide-select.component.html',
    styleUrls: ['./peptide-select.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => PeptideSelectComponent),
            multi: true
        }
    ]
})
export class PeptideSelectComponent implements OnInit, ControlValueAccessor {
    @Input() label: string;
    @Input() tooltip: string;
    @Input() type: string;
    @Input() source: Array<any>;
    @Input() sourceSelect: HTMLSelectElement;
    toggle: boolean = false;
    @ViewChild('select') select: HTMLSelectElement;

    values: Array<any> = [];

    onChange: any;

    constructor() {}
    /**
     * Adds options to the component of choice, removing them from the source array
     * @param to
     * @param options to be added (selected in source)
     */

    ngOnInit() {}

    public add(options: HTMLOptionsCollection) {
        let selected: Array<any> = Array.apply(null, options)
            .filter((o) => o.selected)
            .map((o) => o.value);
        selected.forEach((val) => {
            let element = this.source.filter((v) => v.short === val)[0];
            if (element) {
                this.values.push(element);
                this.source.splice(this.source.indexOf(element), 1);
            } else {
                throw 'Post-translational modification not ofund in list: ' +
                    val;
            }
        });
        this.values.sort(shortNameSort);
        this.onChange(this.values.map((v) => v.value));
        return false;
    }

    /**
     * Removes selected options from a list, returning it to the source array
     * @param from the component the values are removed from
     * @param options options to be removed (selected in out component)
     */
    public remove(options: HTMLOptionsCollection) {
        let removed: Array<any> = Array.apply(null, options)
            .filter((o) => o.selected)
            .map((o) => o.text);
        removed.forEach((val) => {
            let element = this.values.filter((v) => v.short === val)[0];
            if (element) {
                this.values.splice(this.values.indexOf(element), 1);
                this.source.push(element);
            } else {
                throw 'Post-translational modification not ofund in list: ' +
                    val;
            }
        });
        this.source.sort(shortNameSort);
        this.onChange(this.values);
        return false;
    }
    public showWhole(item: HTMLOptionElement) {}

    writeValue(obj: any): void {
        if (obj) {
            this.values = obj;
        }
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {}
    setDisabledState?(isDisabled: boolean): void {
        this.select.toggleAttribute('disabled', isDisabled);
    }

    public toggleTooltip() {
        this.toggle = !this.toggle;
        let state = this.toggle;
        if (this.toggle) {
            setTimeout(() => {
                if (this.toggle === state) {
                    this.toggleTooltip();
                }
            }, 5000);
        }
    }
}
