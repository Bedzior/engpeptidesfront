import {
    Component,
    EventEmitter,
    forwardRef,
    Input,
    OnInit,
    Output
} from '@angular/core';
import {
    ControlValueAccessor,
    Form,
    FormControl,
    NG_VALUE_ACCESSOR
} from '@angular/forms';
import {Router} from '@angular/router';
import {FileItem, FileUploader, FileUploaderOptions} from 'ng2-file-upload';

import {IAppConfig} from '../../app.config';
import {AuthorizationService} from '../../auth/authorization.service';
import {UploadStatus} from './upload-status';

@Component({
    selector: 'app-file-single-upload',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FileSingleUploadComponent),
            multi: true
        }
    ],
    templateUrl: './file-single-upload.component.html',
    styleUrls: ['./file-single-upload.component.scss']
})
// Example from https://github.com/valor-software/ng2-file-upload/blob/master/demo/components/file-upload/zs-file-demo/demo.ts
// MIT License
export class FileSingleUploadComponent implements OnInit, ControlValueAccessor {
    public fileUploadService: FileUploader = new FileUploader({
        url: '/api/secured/upload'
    });
    private autoUpload = true;
    private queueLimit = 1;

    fileHover: boolean = false;

    @Input()
    public name: string;
    @Input()
    public formControl?: FormControl;
    @Input()
    public ngModel?: any;
    @Input()
    public model?: any;
    @Output()
    public ngModelChanged = new EventEmitter<string>();
    // tslint:disable-next-line:no-input-rename
    @Input('maxSize')
    public maxFileSize: number;
    @Input()
    public headers: Array<any>;

    public status: UploadStatus = new UploadStatus();

    private inputs: string[] = [
        'autoUpload',
        'isHTML5',
        'maxFileSize',
        'queueLimit',
        'removeAfterUpload',
        'formatDataFunction'
    ];

    private uploaderOptions: FileUploaderOptions = {};

    constructor(
        appConfig: IAppConfig,
        private router: Router,
        private auth: AuthorizationService
    ) {
        this.uploaderOptions['url'] = appConfig.uploadEndpoint;
    }

    propagateChange = (_: any) => {};

    writeValue(obj: any): void {
        this.ngModel = obj;
    }
    registerOnChange(fn: any): void {
        this.propagateChange = fn;
    }

    registerOnTouched(fn: any): void {}

    ngOnInit() {
        for (let input of this.inputs) {
            if (this[input]) {
                this.uploaderOptions[input] = this[input];
            }
        }
        this.fileUploadService.setOptions(this.uploaderOptions);
        this.fileUploadService.onAfterAddingFile = (f: FileItem) => {
            this.status.start(f);
        };
        this.fileUploadService.onErrorItem = (
            f: FileItem,
            rsp,
            status: number
        ) => {
            this.status.setError();
        };
        this.fileUploadService.onProgressItem = (
            item: FileItem,
            progress: any
        ) => {
            this.status.onProgress(progress);
        };
        this.fileUploadService.onBeforeUploadItem = (item: FileItem) => {
            this.status.start(item);
        };
        this.fileUploadService.onSuccessItem = (item: FileItem, rsp) => {
            this.status.setFinished();
            this.ngModel = JSON.parse(rsp).token;
            if (this.formControl) {
                this.formControl.setValue(this.ngModel);
            }
            console.log('Successfully uploaded ' + item._file.name);
        };
        this.fileUploadService.onBuildItemForm = (
            item: FileItem,
            form: Form
        ) => {
            return {
                name: item._file.name,
                length: item._file.size,
                contentType: item._file.type,
                date: new Date()
            };
        };
    }

    public onFile(event: any): void {
        let files: Array<any> = event.srcElement.files;
        if (files && files.length) {
            this.fileUploadService.addToQueue(event.srcElement.files);
            setTimeout(() => {
                console.log(
                    'Uploading ' + event.srcElement.files[0].name + ' to server'
                );
                this.fileUploadService.uploadAll();
            }, 0);
        }
    }
    public onRemove(event: any): boolean {
        this.fileUploadService.cancelItem(this.status.item);
        this.ngModel = undefined;
        this.status.clear();
        this.fileUploadService.clearQueue();
        if (this.formControl) {
            this.formControl.setValue(null);
        } else {
            this.propagateChange(this.ngModel);
        }
        return false;
    }
    _getTransfer(event: any): any {
        return event.dataTransfer
            ? event.dataTransfer
            : event.originalEvent.dataTransfer;
    }

    _preventAndStop(event: any): any {
        event.preventDefault();
        event.stopPropagation();
    }

    _haveFiles(types: any): any {
        if (!types) {
            return false;
        }
        if (types.indexOf) {
            return types.indexOf('Files') !== -1;
        } else if (types.contains) {
            return types.contains('Files');
        } else {
            return false;
        }
    }

    dragFileAccepted(acceptedFile: FileItem) {}
    dragFileOverStart() {
        this.fileHover = true;
    }

    dragFileOverEnd() {
        this.fileHover = false;
    }
}
