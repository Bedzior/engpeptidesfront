import {async, ComponentFixture, TestBed, inject} from '@angular/core/testing';
import {HttpModule} from '@angular/http';
import {RouterTestingModule} from '@angular/router/testing';
import {FileUploadModule} from 'ng2-file-upload';
import {
    TranslateService,
    TranslateModule,
    TranslateLoader
} from '@ngx-translate/core';

import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {FileSingleUploadComponent} from './file-single-upload.component';
import {AppConfigProvider} from '../../app.config.provider';
import {MockAuthorizationServiceProvider} from 'test/services/authorization.service';

describe('FileSingleUploadComponent', () => {
    let component: FileSingleUploadComponent;
    let fixture: ComponentFixture<FileSingleUploadComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpModule,
                FileUploadModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [
                AppConfigProvider,
                FileSingleUploadComponent,
                MockAuthorizationServiceProvider,
                TranslateService
            ],
            declarations: [FileSingleUploadComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FileSingleUploadComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', inject(
        [FileSingleUploadComponent],
        (_fixture: ComponentFixture<FileSingleUploadComponent>) => {
            expect(component).toBeTruthy();
        }
    ));
});
