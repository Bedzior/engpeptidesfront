import {FileItem} from 'ng2-file-upload';

export class UploadStatus {
    uploading: boolean = false;
    finished: boolean = false;
    error: boolean = false;
    progress: number = 0;
    color: string = '';
    item: FileItem;

    start(item: FileItem) {
        this.finished = false;
        this.error = false;
        this.uploading = true;
        this.progress = 0;
        this.color = 'blue';
        this.item = item;
    }
    onProgress(progress: number) {
        this.uploading = true;
        this.progress = progress;
        this.color = 'linear-gradient(to right, green ' + progress + '%, blue)';
    }
    clear() {
        this.item = undefined;
        this.uploading = false;
        this.finished = false;
        this.error = false;
        this.progress = 0;
        this.color = '';
    }

    setFinished() {
        this.color = 'green';
        this.error = false;
        this.finished = true;
        this.uploading = false;
        this.progress = 100;
    }

    setError() {
        this.finished = false;
        this.error = true;
        this.uploading = false;
        this.color = 'red';
    }
}
