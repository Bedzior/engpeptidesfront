import {NO_ERRORS_SCHEMA} from '@angular/core';
import {async, ComponentFixture, inject, TestBed} from '@angular/core/testing';
import {FormArray, FormBuilder, FormControl} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppConfigProvider} from '../../app.config.provider';
import {FileSeveralUploadComponent} from './file-several-upload.component';
import {MockAuthorizationServiceProvider} from 'test/services/authorization.service';

describe('FileSeveralUploadComponent', () => {
    let component: FileSeveralUploadComponent;
    let fixture: ComponentFixture<FileSeveralUploadComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [
                FormBuilder,
                AppConfigProvider,
                FileSeveralUploadComponent,
                MockAuthorizationServiceProvider
            ],
            declarations: [FileSeveralUploadComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FileSeveralUploadComponent);
        component = fixture.componentInstance;
        component.model = ['abc', 'def'];
        component.formControl = new FormArray([]);
        component.formControl.push(new FormControl('abc'));
        component.formControl.push(new FormControl('def'));
        fixture.detectChanges();
    });

    it('should create', inject(
        [FileSeveralUploadComponent],
        (_fixture: ComponentFixture<FileSeveralUploadComponent>) => {
            expect(component).toBeTruthy();
        }
    ));
});
