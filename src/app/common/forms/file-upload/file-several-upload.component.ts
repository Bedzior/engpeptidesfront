import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, Validators} from '@angular/forms';

@Component({
    selector: 'app-file-several-upload',
    templateUrl: './file-several-upload.component.html',
    styleUrls: ['./file-several-upload.component.scss']
})
export class FileSeveralUploadComponent implements OnInit {
    @Input()
    public id: string;
    @Input()
    public name: string;
    @Input()
    public headers: Array<any>;
    // tslint:disable-next-line:no-input-rename
    @Input('maxSize')
    public maxFileSize: number;

    // tslint:disable-next-line:no-input-rename
    @Input('formArray')
    public formControl: FormArray;
    public model?: Array<string> = [];

    constructor(private fb: FormBuilder) {}

    ngOnInit() {}
    updateList(token, index) {
        if (token) {
            this.formControl.push(
                this.fb.control(null, Validators.nullValidator)
            ); // no requirement for more than 1 file
            this.model.push(token);
        } else if (index !== undefined) {
            if (this.model.length > index) {
                this.formControl.controls.splice(index, 1);
                this.model.splice(index, 1);
            }
        }
    }
}
