import {ClassProvider} from '@angular/core';

import {IdentityService} from './identity.service';
import {IdentityServiceImpl} from './private/identity.service';

export const IdentityServiceProvider: ClassProvider = {
    provide: IdentityService,
    useClass: IdentityServiceImpl
};
