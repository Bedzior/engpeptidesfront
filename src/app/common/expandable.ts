export interface Expandable {
    expand(): boolean;
    collapse(): boolean;
}
