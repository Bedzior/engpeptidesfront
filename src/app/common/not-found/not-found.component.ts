import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-not-found',
    template: `
        <p translate>NAVI.PAGE_NOT_FOUND</p>
        <p><a [routerLink]="['/login']" translate>NAVI.RETURN_HOME</a></p>
    `
})
export class NotFoundComponent implements OnInit {
    constructor(router: Router) {}

    ngOnInit() {}
}
