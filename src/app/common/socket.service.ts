import {Observable} from 'rxjs';

export abstract class SocketService {
    abstract disconnect(): void;
    abstract subscribe(queue: string): Observable<{}>;
    abstract unsubscribe(): void;
    abstract publish(queue: string, body: {}): void;
}
