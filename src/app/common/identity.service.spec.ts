import {inject, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {AppConfigProvider} from 'app/common/app.config.provider';
import {IdentityService} from './identity.service';

describe('IIdentityService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [IdentityService, AppConfigProvider]
        });
    });

    it('should ...', inject([IdentityService], (service: IdentityService) => {
        expect(service).toBeTruthy();
    }));
});
