import {ClassProvider} from '@angular/core';

import {IAppConfig} from './app.config';
import {AppConfigImpl} from './private/app.config';

export const AppConfigProvider: ClassProvider = {
    provide: IAppConfig,
    useClass: AppConfigImpl
};
