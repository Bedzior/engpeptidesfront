export class StatusResponse {
    success: boolean;
    message: string;
}

export class ErrorResponse extends StatusResponse {
    token?: string;
    errors?: string[];
}
