import {
    AnimationStyleMetadata,
    style,
    state,
    animate,
    transition,
    trigger
} from '@angular/animations';

let SHORT_TIME = 150;
let MEDIUM_TIME = 300;
let LONG_TIME = 800;
export let tooltipContainerInitial: AnimationStyleMetadata = style({
    opacity: 0,
    width: 0,
    height: '*'
});
export let tooltipContainerActive: AnimationStyleMetadata = style({
    opacity: 1,
    width: '*',
    height: '*'
});
export let tooltipTextInitial: AnimationStyleMetadata = style({
    color: 'transparent'
});
export let tooltipTextActive: AnimationStyleMetadata = style({
    color: '#6BB9AA'
});
let placeholderInitial: AnimationStyleMetadata = style({
    opacity: 0,
    color: '#888'
});
let placeholderActive: AnimationStyleMetadata = style({
    opacity: 1,
    color: '#BAE1D9'
});
let tooltipSwitchTime = SHORT_TIME;
let tooltipTextSwitch =
    tooltipSwitchTime + 'ms ' + SHORT_TIME + 'ms ease-in-out';
// 'app-reactive-input' placeholder animations
let placeholderSwitchTime = MEDIUM_TIME;
export let placeholderSwitch = trigger('placeholderSwitch', [
    transition(':enter', [
        placeholderInitial,
        animate(placeholderSwitchTime, placeholderActive)
    ]),
    transition(':leave', [animate(placeholderSwitchTime, placeholderInitial)])
]);
export let tooltipSwitch = trigger('tooltipSwitch', [
    transition(':enter', [
        tooltipContainerInitial,
        tooltipTextInitial,
        animate(tooltipSwitchTime, tooltipContainerActive),
        animate(tooltipTextSwitch, tooltipTextActive)
    ]),
    transition(':leave', [
        animate(tooltipSwitchTime, tooltipTextInitial),
        animate(tooltipTextSwitch, tooltipContainerInitial)
    ])
]);

// 'floatbox' animation leave/enter
let floatboxSlideTime = LONG_TIME;
let floatboxSlideOut = style({
    transform: 'translate(-50%,0%)',
    opacity: 0
});
let floatboxDocked = style({
    transform: 'translate(0%,0%)',
    opacity: 1
});
let floatboxInPlace = style({
    marginTop: '*'
});
export let floatboxExit = trigger('floatboxExit', [
    state('in', floatboxInPlace),
    transition(':enter', [
        floatboxSlideOut,
        animate(floatboxSlideTime + 'ms 0ms ease-out', floatboxDocked),
        animate(0, floatboxInPlace)
    ]),
    transition(':leave', [
        floatboxDocked,
        animate(floatboxSlideTime + 'ms 0ms ease-in', floatboxSlideOut)
    ])
]);

// 'underneath' router outlet
let underneathSwitchTime = MEDIUM_TIME;
let underneathHidden = style({
    opacity: 0
});
let underneathVisible = style({
    opacity: 1
});
export let floatboxCover = trigger('floatboxCover', [
    transition(':enter', [
        underneathHidden,
        animate(
            floatboxSlideTime + 'ms ' + underneathSwitchTime + 'ms ease-out',
            underneathVisible
        )
    ]),
    transition(':leave', [
        animate(underneathSwitchTime + 'ms 0ms ease-in', underneathHidden)
    ])
]);

let inputShrinkTime = SHORT_TIME;
export let inputShrink = trigger('inputShrink', [
    transition('extend => *', [
        animate(inputShrinkTime + 'ms 0ms ease-in', style({width: '*'}))
    ]),
    transition('* => extend', [
        style({width: '*'}),
        animate(inputShrinkTime + 'ms 0ms ease-in', style({width: '100%'}))
    ]),
    transition(':leave', [
        animate(inputShrinkTime + 'ms 0ms ease-in', style({width: 0}))
    ]),
    transition(':enter', [
        style({width: 0}),
        animate(inputShrinkTime + 'ms 0ms ease-in', style({width: '*'}))
    ])
]);
