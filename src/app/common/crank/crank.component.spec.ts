import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CrankComponent} from './crank.component';

describe('CrankComponent', () => {
    let component: CrankComponent<any>;
    let fixture: ComponentFixture<CrankComponent<any>>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CrankComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CrankComponent);
        component = fixture.componentInstance;
        component.states = ['abc', 'def'];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
