import {
    AfterViewInit,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output,
    QueryList,
    Renderer,
    ViewChildren
} from '@angular/core';
import {ElementRef} from '@angular/core/src/linker/element_ref';

@Component({
    selector: 'app-crank',
    templateUrl: './crank.component.html',
    styleUrls: ['./crank.component.scss']
})
export class CrankComponent<T> implements OnInit, AfterViewInit {
    @Input()
    r: number = 50;
    @Input()
    border: number = 5;
    /**
     * In case we want to add some initial rotation to the component's path, in `deg`.
     * Default: 0
     */
    @Input()
    startRotation: number = 0;

    startRotationRads: number = this.getRads(this.startRotation);

    stateValue: T;
    @Output()
    stateChanged: EventEmitter<T> = new EventEmitter(true);
    @Input()
    states: Array<T>;

    @ViewChildren('textElements')
    textElements: QueryList<ElementRef>;
    stateIndex;

    x: number;
    y: number;
    degs: number;
    rads: number;

    constructor(private renderer: Renderer) {}

    ngAfterViewInit(): void {
        let centerXOffset = (width: number, i: number) => {
            return (
                (-width *
                    Math.sin(this.startRotationRads + this.rads * (i + 0.5))) /
                2
            );
        };
        let centerYOffset = (height: number, i: number) => {
            return (
                (height *
                    Math.cos(this.startRotationRads + this.rads * (i + 0.5))) /
                2
            );
        };
        this.textElements.forEach((item: ElementRef, i: number) => {
            let bounds = item.nativeElement.getBoundingClientRect();
            let width = bounds.width;
            let height = bounds.height;
            this.renderer.setElementAttribute(
                item.nativeElement,
                'x',
                (
                    this.getX(
                        this.rads * (i + 0.5) + this.startRotationRads,
                        ((this.r + this.border) * 2) / 3
                    ) + centerXOffset(width, i)
                ).toString()
            );
            this.renderer.setElementAttribute(
                item.nativeElement,
                'y',
                (
                    this.getY(
                        this.rads * (i + 0.5) + this.startRotationRads,
                        ((this.r + this.border) * 2) / 3
                    ) + centerYOffset(height, i)
                ).toString()
            );
        });
    }

    ngOnInit() {
        this.degs = 360 / this.states.length;
        this.rads = this.getRads(this.degs);
        this.x = this.getX(this.rads, this.r);
        this.y = this.getY(this.rads, this.r);
        this.startRotationRads = this.getRads(this.startRotation);
    }
    private emit(): any {
        this.stateChanged.emit(this.stateValue);
    }

    rotate() {
        this.state = this.states[++this.stateIndex % this.states.length];
    }
    getX(rads, r) {
        return this.r + this.border + r * Math.sin(rads);
    }
    getY(rads, r) {
        return this.r + this.border - r * Math.cos(rads);
    }

    getRads(degs) {
        return (degs * Math.PI) / 180;
    }

    get state() {
        return this.stateValue;
    }
    @Input()
    set state(state: T) {
        if (this.stateValue !== state) {
            let stateIndex = this.states.indexOf(state);
            if (stateIndex >= 0) {
                if (
                    stateIndex % this.states.length !==
                    this.stateIndex % this.states.length
                ) {
                    this.stateIndex = stateIndex;
                }
                this.stateValue = state;
                this.emit();
            }
        }
    }
}
