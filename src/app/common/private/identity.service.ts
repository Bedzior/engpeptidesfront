import {Injectable, Injector} from '@angular/core';

import {IAppConfig} from '../app.config';
import {APIService} from '../api.service';
import {IdentityService} from '../identity.service';

const endpoint = '/secured/identity';

@Injectable()
export class IdentityServiceImpl extends APIService implements IdentityService {
    constructor(conf: IAppConfig, injector: Injector) {
        super(conf, injector);
    }

    public update(form: {}): Promise<{}> {
        return this.post(endpoint, form);
    }
}
