import {IAppConfig} from '../app.config';

export class AppConfigImpl implements IAppConfig {
    apiEndpoint = '/api';
    socketEndpoint = '/api/secured/socket';
    uploadEndpoint = '/api/secured/upload';
}
