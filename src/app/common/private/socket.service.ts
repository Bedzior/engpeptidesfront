import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {StompRService} from '@stomp/ng2-stompjs';
import {Message} from '@stomp/stompjs';
import {CookieService} from 'angular2-cookie';

import {COOKIE_AUTH} from '../auth/authorization.service';
import {SocketService} from '../socket.service';

@Injectable()
export class SocketServiceImpl implements SocketService {
    protected subscribed: boolean;

    constructor(
        private stompService: StompRService,
        private cookies: CookieService
    ) {}

    private connect(): void {
        this.stompService.config = {
            url: 'ws://localhost:8080/api/ws/websocket',
            headers: {
                Authentication: this.cookies.get(COOKIE_AUTH)
            },
            heartbeat_in: 0,
            heartbeat_out: 10000,
            reconnect_delay: 5000,
            debug: true
        };
        this.stompService.initAndConnect();
    }

    public disconnect(): void {
        if (this.stompService.connected()) {
            this.stompService.disconnect();
        }
    }

    public subscribe(queue: string): Observable<{}> {
        if (!this.stompService.connected()) {
            this.connect();
        }
        if (!this.subscribed) {
            this.subscribed = true;
        }
        return this.stompService
            .subscribe(queue)
            .takeWhile(() => this.subscribed)
            .map((message: Message) => {
                return JSON.parse(message.body);
            });
    }
    public unsubscribe() {
        this.subscribed = false;
    }
    public publish(queue: string, body: {}): void {
        if (!this.stompService.connected()) {
            this.connect();
        }
        this.stompService.publish(queue, JSON.stringify(body));
    }
}
