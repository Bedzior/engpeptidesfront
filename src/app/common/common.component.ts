import {Subscription} from 'rxjs/Subscription';
import {Component, OnDestroy} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRouteSnapshot, NavigationEnd, Router} from '@angular/router';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';

@Component({
    selector: 'app-common',
    template: ''
})
export class AppCommonComponent implements OnDestroy {
    private subscription: Subscription;
    private langSubscription: Subscription;

    constructor(
        private titleService: Title,
        private router: Router,
        private translate: TranslateService
    ) {
        this.subscription = this.router.events
            .filter((e) => e instanceof NavigationEnd)
            .subscribe((e) => {
                this.determineTitle();
            });
        this.langSubscription = this.translate.onLangChange.subscribe(
            (event: LangChangeEvent) => {
                this.determineTitle();
            }
        );
    }
    private determineTitle() {
        if (this.router.routerState.root.firstChild) {
            let title = this.getDeepestTitle(
                this.router.routerState.snapshot.root
            );
            this.titleService.setTitle(
                this.translate.instant('TITLES.' + title)
            );
        }
    }
    private getDeepestTitle(routeSnapshot: ActivatedRouteSnapshot) {
        let title = routeSnapshot.data ? routeSnapshot.data['title'] : '';
        if (routeSnapshot.firstChild) {
            title = this.getDeepestTitle(routeSnapshot.firstChild) || title;
        }
        return title;
    }
    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
        if (this.langSubscription) {
            this.langSubscription.unsubscribe();
        }
    }
}
