import {inject, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {AuthorizationService} from 'app/common';
import {AppConfigProvider} from '../app.config.provider';

describe('IAuthorizationService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [AuthorizationService, AppConfigProvider]
        });
    });

    it('should ...', inject(
        [AuthorizationService],
        (service: AuthorizationService) => {
            expect(service).toBeTruthy();
        }
    ));
});
