import {ClassProvider} from '@angular/core';

import {AuthorizationService} from './authorization.service';
import {AuthorizationServiceImpl} from './private/authorization.service';

export const AuthorizationServiceProvider: ClassProvider = {
    provide: AuthorizationService,
    useClass: AuthorizationServiceImpl
};
