import {Injectable} from '@angular/core';
import {
    ActivatedRoute,
    CanActivate,
    CanActivateChild,
    Router
} from '@angular/router';

import {AuthorizationService} from './authorization.service';

@Injectable()
export class LoggedInGuard implements CanActivate, CanActivateChild {
    constructor(
        private authService: AuthorizationService,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    public canActivate() {
        try {
            let user = this.authService.getUser();
            return true;
        } catch (e) {
            console.log('User not logged in; redirecting to login page');
            if (this.router.url !== '/login') {
                if (this.router.url === '/') {
                    this.router.navigate(['login']);
                } else {
                    this.router.navigate(['login', {return: this.router.url}]);
                }
            }
            return false;
        }
    }

    public canActivateChild() {
        return this.canActivate();
    }
}
