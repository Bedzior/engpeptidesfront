import {UserModel} from '../user/user.model';

export const COOKIE_AUTH = 'Authentication';

export abstract class AuthorizationService {
    abstract loggedIn: boolean;

    abstract getUser(): UserModel;

    abstract token(): string;
    abstract getLogin(): string;

    abstract hasRole(role: string): boolean;
    abstract async checkSession(): Promise<boolean>;
    abstract login(form: JSON): Promise<UserModel>;
    /**
     *  Logs the user out from server's side, removing local data
     */
    abstract logout(): Promise<{}>;
}
