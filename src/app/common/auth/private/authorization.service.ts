import {Injectable, Injector} from '@angular/core';
import {Headers} from '@angular/http';
import {JwtHelper} from 'angular2-jwt';

import {APIService} from '../../api.service';
import {IAppConfig} from '../../app.config';
import {UserModel} from '../../user/user.model';
import {AuthorizationService, COOKIE_AUTH} from '../authorization.service';

const loginEndpoint = '/login';
const logoutEndpoint = '/logout';
const sessionEndpoint = '/session';

const USER_STORAGE = 'usr';
const helper = new JwtHelper();

@Injectable()
export class AuthorizationServiceImpl extends APIService
    implements AuthorizationService {
    public loggedIn: boolean = false;
    private user: UserModel;

    constructor(conf: IAppConfig, injector: Injector) {
        super(conf, injector);
    }

    public getUser(): UserModel {
        if (!this.user) {
            let userFromStorage = localStorage.getItem(USER_STORAGE);
            if (userFromStorage) {
                this.user = new UserModel(userFromStorage);
            }
        }
        if (this.user) {
            return this.user;
        }
        throw new Error('No user in session');
    }

    public token(): string {
        let authorization = this.cookieService.get(COOKIE_AUTH)[0];
        if (authorization) {
            return authorization[0].substring('Bearer '.length);
        }
        return undefined;
    }
    public getLogin(): string {
        return this.user ? this.user.getLogin() : undefined;
    }

    public hasRole(role: string): boolean {
        let user = this.getUser();
        if (user) {
            return user.getRoles().indexOf(role) >= 0;
        }
        return false;
    }

    /**
     * Called initially from LoggedInGuard to check for a pre-existing session
     */
    public async checkSession(): Promise<boolean> {
        return await this.get(sessionEndpoint)
            .then((rsp: {}) => {
                if (!rsp) {
                    this.clearSession();
                    return false;
                } else {
                    this.store(rsp);
                    return true;
                }
            })
            .catch((err) => {
                console.log('Error retrieving session status', err);
                if (err.status !== 504) {
                    this.clearSession();
                }
                return false;
            });
    }
    private clearSession(): void {
        this.user = null;
        this.cookieService.remove(COOKIE_AUTH);
        localStorage.removeItem(USER_STORAGE);
        this.loggedIn = false;
    }
    public login(form: JSON): Promise<UserModel> {
        return this.post(loginEndpoint, form, true).then(
            (rsp: [{}, Headers]) => {
                if (rsp[1]) {
                    let authorization = rsp[1].get('authorization');
                    let exp = helper.getTokenExpirationDate(
                        authorization.substring('Bearer '.length)
                    );
                    this.cookieService.put(COOKIE_AUTH, authorization, {
                        expires: exp
                    });
                }
                this.store(rsp[0]);
                return this.user;
            }
        );
    }

    private store(rsp: {}) {
        let userJSON = JSON.stringify(rsp);
        this.user = new UserModel(userJSON);
        localStorage.setItem(USER_STORAGE, userJSON);
        this.loggedIn = true;
    }

    public logout(): Promise<{}> {
        return this.post(logoutEndpoint).then(
            (response) => {
                console.info(this.user.getLogin() + ' was logged out');
                this.clearSession();
                return response;
            },
            (error) => {
                this.clearSession();
                return error;
            }
        );
    }
}
