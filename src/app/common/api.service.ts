import {Injector} from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {CookieService} from 'angular2-cookie';

import {IAppConfig} from './app.config';
import {COOKIE_AUTH} from './auth/authorization.service';

/***
 * Abstract class any API service should extend from
 */
export abstract class APIService {
    protected cookieService: CookieService;
    protected http: Http;
    constructor(protected conf: IAppConfig, protected injector: Injector) {
        this.cookieService = injector.get(CookieService);
        this.http = injector.get(Http);
    }

    protected post(
        endpoint: string,
        data?: any,
        withHeaders?: boolean
    ): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .post(
                    this.conf.apiEndpoint + endpoint,
                    data,
                    new RequestOptions({
                        headers: new Headers(
                            this.extendWithAuth({
                                Accept: 'application/json',
                                'Content-Type':
                                    'application/json; charset=utf-8',
                                'Access-Control-Allow-Origin': '*',
                                'Access-Control-Allow-Credentials': 'true',
                                'Cache-Control': 'no-cache'
                            })
                        )
                    })
                )
                .subscribe(
                    (rsp: Response) => {
                        let response = rsp.text();
                        if (
                            response.startsWith('{') ||
                            response.startsWith('[')
                        ) {
                            response = rsp.json();
                        }
                        if (withHeaders) {
                            resolve([response, rsp.headers]);
                        } else {
                            resolve(response);
                        }
                    },
                    (err: Response) => {
                        console.log(err);
                        let contentType = err.headers.get('content-type');
                        if (contentType && contentType.startsWith('application/json')) {
                            if (withHeaders) {
                                reject([err.json(), err.headers]);
                            } else {
                                reject(err.json());
                            }
                        } else {
                            if (withHeaders) {
                                reject([{
                                    message: err.statusText,
                                    status: err.status
                                }, err.headers]);
                            } else {
                                reject({
                                    message: err.statusText,
                                    status: err.status
                                });
                            }
                        }
                    }
                );
        });
    }

    protected get(endpoint: string, withHeaders?: boolean): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .get(this.conf.apiEndpoint + endpoint, {
                    headers: new Headers(
                        this.extendWithAuth({
                            Accept: 'application/json',
                            'Access-Control-Allow-Origin': '*'
                        })
                    )
                })
                .subscribe(
                    (rsp: Response) => {
                        let response = rsp.text();
                        if (
                            response.startsWith('{') ||
                            response.startsWith('[')
                        ) {
                            response = rsp.json();
                        }
                        if (withHeaders) {
                            resolve([response, rsp.headers]);
                        } else {
                            resolve(response);
                        }
                    },
                    (err: Response) => {
                        console.log(err);
                        let contentType = err.headers.get('content-type');
                        if (
                            contentType &&
                            contentType.startsWith('application/json')
                        ) {
                            reject(err.json());
                        } else {
                            reject({
                                message: err.statusText,
                                status: err.status
                            });
                        }
                    }
                );
        });
    }

    protected put(endpoint: string, data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .put(
                    this.conf.apiEndpoint + endpoint,
                    data,
                    new RequestOptions({
                        headers: new Headers(
                            this.extendWithAuth({
                                Accept: 'application/json',
                                'Content-Type':
                                    'application/json; charset=utf-8',
                                'Access-Control-Allow-Origin': '*',
                                'Access-Control-Allow-Credentials': 'true',
                                'Cache-Control': 'no-cache'
                            })
                        )
                    })
                )
                .subscribe(
                    (rsp: Response) => {
                        resolve(rsp.json());
                    },
                    (err: Response) => {
                        console.log(err);
                        let contentType = err.headers.get('content-type');
                        if (
                            contentType &&
                            contentType.startsWith('application/json')
                        ) {
                            reject(err.json());
                        } else {
                            reject({
                                message: err.statusText,
                                status: err.status
                            });
                        }
                    }
                );
        });
    }

    protected delete(endpoint: string, id: any): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http
                .delete(
                    this.conf.apiEndpoint + endpoint + '/' + id,
                    new RequestOptions({
                        headers: new Headers(
                            this.extendWithAuth({
                                'Access-Control-Allow-Origin': '*',
                                'Access-Control-Allow-Credentials': 'true',
                                'Cache-Control': 'no-cache'
                            })
                        )
                    })
                )
                .subscribe(
                    (rsp: Response) => {
                        resolve(rsp.json());
                    },
                    (err: Response) => {
                        console.log(err);
                        let contentType = err.headers.get('content-type');
                        if (
                            contentType &&
                            contentType.startsWith('application/json')
                        ) {
                            reject(err.json());
                        } else {
                            reject({
                                message: err.statusText,
                                status: err.status
                            });
                        }
                    }
                );
        });
    }

    private extendWithAuth(obj: {}): {} {
        let cookie = this.cookieService.get(COOKIE_AUTH);
        if (cookie) {
            obj['Authentication'] = cookie;
        }
        return obj;
    }
}
