export let backendValues = (a, b) => {
    let nameA = '',
        nameB = '';
    if (a.name && b.name) {
        nameA = a.name.toUpperCase();
        nameB = b.name.toUpperCase();
    } else if (a.short && b.short) {
        nameA = a.short.toUpperCase();
        nameB = b.short.toUpperCase();
    } else if (typeof a === 'string' && typeof b === 'string') {
        nameA = a.toUpperCase();
        nameB = b.toUpperCase();
    }
    if (nameA < nameB) {
        return -1;
    } else if (nameA > nameB) {
        return 1;
    } else {
        return 0;
    }
};

export let shortNameSort = (a, b) => {
    let nameA = a.short.toUpperCase();
    let nameB = b.short.toUpperCase();
    if (nameA < nameB) {
        return -1;
    } else if (nameA > nameB) {
        return 1;
    } else {
        return 0;
    }
};
