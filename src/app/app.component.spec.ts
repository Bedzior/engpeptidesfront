import {NO_ERRORS_SCHEMA, EventEmitter} from '@angular/core';
import {
    TestBed,
    async,
    ComponentFixture
} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
    TranslateModule,
    TranslateLoader,
    TranslateService,
    TranslateFakeLoader
} from '@ngx-translate/core';

import {AppComponent} from 'app';
import {MockAuthorizationServiceProvider} from 'test/services/authorization.service';

describe('AppComponent', () => {
    let component: AppComponent;
    let fixture: ComponentFixture<AppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                BrowserAnimationsModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: TranslateFakeLoader
                    }
                })
            ],
            providers: [MockAuthorizationServiceProvider, TranslateService],
            declarations: [AppComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create the app', () => {
        expect(component).toBeTruthy();
    });

    it('should render a floating box', () => {
        let compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('.floatbox')).toBeTruthy();
    });
});
