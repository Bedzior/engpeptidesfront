import {Subscription} from 'rxjs';
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Response} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {AuthorizationService} from 'app/common';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
    public form: FormGroup;
    private subscription: Subscription;
    private returnURL: string;

    constructor(
        @Inject(FormBuilder) private fb: FormBuilder,
        private authService: AuthorizationService,
        private router: Router,
        private route: ActivatedRoute,
        private translate: TranslateService
    ) {
        this.form = this.fb.group({
            j_username: [null, Validators.required],
            j_password: [null, Validators.required]
        });
    }

    ngOnInit() {
        try {
            if (this.authService.getUser()) {
                this.router.navigateByUrl('/config');
                return;
            }
        } catch (e) {}
        this.subscription = this.route.queryParams.subscribe((params: any) => {
            this.returnURL = params['return'];
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    submit(event: Event) {
        event.preventDefault();
        if (!this.form.valid) {
            return;
        }
        this.form.disable();
        this.authService.login(this.form.value).then(
            (rsp) => {
                this.form.enable();
                let returnURL = this.returnURL ? this.returnURL : '/config';
                console.log(
                    'Login for',
                    this.form.value['j_username'],
                    'successful; redirecting to',
                    returnURL
                );
                this.router.navigateByUrl(returnURL).then(
                    () => {
                        console.log('success');
                    },
                    () => {
                        console.log('failed');
                    }
                );
                return true;
            },
            (rsp: [Response, Headers]) => {
                this.form.enable();
                setTimeout(() => {
                    if (rsp[0].status === 401) {
                        alert(
                            this.translate.instant(
                                'LOGIN.MESSAGE.ERR_INCORRECT'
                            )
                        );
                    } else {
                        alert(
                            this.translate.instant(
                                'LOGIN.MESSAGE.ERR_UNEXPECTED'
                            )
                        );
                    }
                }, 0);
            }
        );
    }
}
