import {DebugElement, NO_ERRORS_SCHEMA} from '@angular/core';
import {
    ComponentFixture,
    fakeAsync,
    inject,
    TestBed,
    tick
} from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material';
import {By} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {
    TranslateLoader,
    TranslateModule,
    TranslateService
} from '@ngx-translate/core';

import {AuthorizationService} from 'app/common';
import {LoginComponent} from 'app/pre-login';
import {WebpackTranslateLoader} from 'app/webpack-translate-loader';
import {
    MockAuthorizationService,
    MockAuthorizationServiceProvider
} from 'test/services/authorization.service';

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let auth: MockAuthorizationService;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                MatProgressSpinnerModule,
                ReactiveFormsModule,
                FormsModule,
                RouterTestingModule,
                TranslateModule.forRoot({
                    loader: {
                        provide: TranslateLoader,
                        useClass: WebpackTranslateLoader
                    }
                })
            ],
            providers: [MockAuthorizationServiceProvider, TranslateService],
            declarations: [LoginComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();

        fakeAsync(
            inject([LoginComponent], (component: LoginComponent) => {
                component.ngOnInit();
                tick();
                expect(component.form).toBeTruthy();
            })
        );
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();

        auth = TestBed.get(AuthorizationService);
        spyOn(auth, 'getUser').and.throwError('No user');
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
    describe('form', () => {
        let form: DebugElement;
        beforeEach(() => {
            form = fixture.debugElement.query(By.css('form'));
        });
        it('should exist', () => {
            expect(form && form.nativeElement).toBeTruthy();
            expect(form.query(By.css('[name="j_username"]'))).toBeTruthy();
            expect(form.query(By.css('[name="j_password"]'))).toBeTruthy();
        });
        it('should allow for interaction', () => {
            expect(form.query(By.css('[type="submit"]'))).toBeTruthy();
        });
    });
});
