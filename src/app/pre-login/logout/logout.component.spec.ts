import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpModule} from '@angular/http';
import {MatProgressSpinnerModule} from '@angular/material';
import {RouterTestingModule} from '@angular/router/testing';

import {AppConfigProvider} from 'app/common/app.config.provider';
import {MockAuthorizationServiceProvider} from 'test/services';
import {LogoutComponent} from './logout.component';

describe('LogoutComponent', () => {
    let component: LogoutComponent;
    let fixture: ComponentFixture<LogoutComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpModule,
                MatProgressSpinnerModule
            ],
            providers: [MockAuthorizationServiceProvider, AppConfigProvider],
            declarations: [LogoutComponent]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(LogoutComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain a message for the user', () => {
        expect(fixture.nativeElement.innerText).toContain('logging out…');
    });

    describe('LogoutComponent.logout', () => {
        // beforeEach(() => {
        //     spyOn(authService, 'logout').and.callThrough();
        // });
        // it('should have called the logout method', () => {
        //     fakeAsync(inject([LogoutComponent, Router], (_component: LogoutComponent, router: Router) => {
        //         spyOn(router, 'navigate').and.callThrough();
        //         _component.ngOnInit();
        //         tick();
        //         expect(authService.logout).toHaveBeenCalled();
        //         expect(router.navigate).toHaveBeenCalledWith(['/msg', { type: 'logout' }]);
        //     }));
        // });
    });
});
