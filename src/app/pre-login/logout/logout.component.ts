import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthorizationService } from 'app/common';

@Component( {
    selector: 'app-logout',
    template: `
    logging out&hellip;
        <br/>
        <mat-progress-spinner
            mode="indeterminate"
            aria-label="logging out&hellip;">
        </mat-progress-spinner>
    `,
    styles: ['']
})
export class LogoutComponent implements OnInit {

    constructor(
        private auth: AuthorizationService,
        private router: Router ) {
    }

    ngOnInit() {
        this.auth.logout().then( rsp => {
            this.router.navigate( ['/msg', { 'type': 'logout' }] );
        }).catch( rsp => {
            this.router.navigate( ['/msg', { 'type': 'logout' }] );
        });
    }

}
