import {TestBed, inject} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {AppConfigProvider} from '../../common/app.config.provider';
import {RegisterService} from './register.service';

describe('RegisterService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpModule],
            providers: [RegisterService, AppConfigProvider]
        });
    });

    it('should ...', inject([RegisterService], (service: RegisterService) => {
        expect(service).toBeTruthy();
    }));
});
