import {animate, state, style, transition, trigger} from '@angular/animations';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Headers, Http} from '@angular/http';
import {Router, ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

import {ErrorResponse, MyValidators} from 'app/common';
import {RegisterService} from './register.service';

@Component({
    selector: 'app-register',
    animations: [
        trigger('inputError', [
            state(
                'true',
                style({
                    width: '*'
                })
            ),
            state(
                'false',
                style({
                    width: 0
                })
            ),
            transition('true => false', animate('300ms 10ms ease-in')),
            transition('false => true', animate('300ms 10ms ease-out'))
        ])
    ],
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    public form: FormGroup;

    constructor(
        private regService: RegisterService,
        private router: Router,
        private route: ActivatedRoute,
        private http: Http,
        private translate: TranslateService,
        @Inject(FormBuilder) fb: FormBuilder
    ) {
        route.queryParams.subscribe((params) => {
            let uuid = params['uuid'];
            if (uuid) {
                this.regService.confirm(uuid, true).then(
                    () => {
                        this.router.navigate([
                            '/msg',
                            {
                                type: 'confirmed',
                                link: {
                                    route: ['login'],
                                    name: 'LOGIN_PAGE'
                                }
                            }
                        ]);
                    },
                    (err) => {
                        debugger;
                    }
                );
            }
        });
        this.form = fb.group({
            login: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(6)
                ])
            ],
            password: [
                null,
                Validators.compose([
                    Validators.required,
                    Validators.minLength(8),
                    MyValidators.properPassword
                ])
            ],
            passwordRepeat: [
                null,
                Validators.compose([MyValidators.equalTo('password')])
            ],
            email: [null, Validators.compose([MyValidators.email])]
        });
    }

    ngOnInit() {}

    submit(event: any): boolean {
        event.preventDefault();
        if (this.form.valid) {
            this.form.disable();
            let formValue = this.form.value;
            delete formValue['passwordRepeat'];
            this.regService
                .register(formValue, true)
                .then((rsp) => {
                    this.router.navigate([
                        '/login',
                        {user: this.form.value.username}
                    ]);
                })
                .catch((err: [ErrorResponse, Headers]) => {
                    this.form.enable();
                    let response = err[0];
                    console.error('Error creating user:', response.message);
                    alert(
                        this.translate.instant(
                            'REGISTRATION.FORM.MESSAGE.' + response.token
                        )
                    );
                });
        }
        return false;
    }
}
