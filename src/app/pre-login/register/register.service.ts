import {ClassProvider, Injectable, Injector} from '@angular/core';
import {Response} from '@angular/http';

import {APIService, IAppConfig} from 'app/common';

const endpoint = '/register';

export abstract class RegisterService {
    abstract register(form: JSON, withHeaders?: boolean): Promise<Response>;
    abstract confirm(uuid: string, withHeaders?: boolean): Promise<Response>;
}

@Injectable()
export class RegisterServiceImpl extends APIService implements RegisterService {
    constructor(conf: IAppConfig, injector: Injector) {
        super(conf, injector);
    }

    public register(form: JSON, withHeaders?: boolean): Promise<Response> {
        return this.post(endpoint, form, withHeaders);
    }

    public confirm(uuid: string, withHeaders?: boolean): Promise<Response> {
        return this.post(endpoint + '/confirm?uuid='+uuid, null, withHeaders);
    }
}

export const RegisterServiceProvider: ClassProvider = {
    provide: RegisterService,
    useClass: RegisterServiceImpl
};
