export {
    RegisterService,
    RegisterServiceProvider
} from './register/register.service';
export * from './register/register.component';
export {
    ResetPasswordService,
    ResetPasswordServiceProvider
} from './reset-password/reset-password.service';
export * from './reset-password/reset-password.component';
export * from './login/login.component';
export * from './logout/logout.component';
