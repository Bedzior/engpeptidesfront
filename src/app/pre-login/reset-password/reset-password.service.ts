import {ClassProvider, Injectable, Injector} from '@angular/core';
import {Response} from '@angular/http';

import {APIService, IAppConfig} from 'app/common';

export abstract class ResetPasswordService {
    abstract reset(form: JSON): Promise<Response>;
    abstract submit(uuid: string): Promise<Response>;
}

@Injectable()
export class ResetPasswordServiceImpl extends APIService
    implements ResetPasswordService {
    constructor(conf: IAppConfig, injector: Injector) {
        super(conf, injector);
    }

    public reset(form: JSON): Promise<Response> {
        return this.post('/resetPasswordRequest', form);
    }
    public submit(uuid: string): Promise<Response> {
        return this.post('/resetPassword?uuid=' + uuid, null);
    }
}

export const ResetPasswordServiceProvider: ClassProvider = {
    provide: ResetPasswordService,
    useClass: ResetPasswordServiceImpl
};
