import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Headers} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';

import {MyValidators} from 'app/common';
import {ResetPasswordService} from '../reset-password/reset-password.service';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    form: FormGroup;

    private subscription: any;

    private username: string;

    public uuid: string;

    public full: boolean = false;

    public email: boolean = false;

    constructor(
        @Inject(FormBuilder) fb: FormBuilder,
        private resetPassService: ResetPasswordService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        if (this.full) {
            this.form = fb.group({
                uuid: [this.uuid],
                newPassword: [
                    '',
                    Validators.compose([MyValidators.properPassword])
                ],
                newPasswordRepeat: [
                    '',
                    Validators.compose([MyValidators.equalTo('newPassword')])
                ]
            });
        } else {
            this.form = fb.group({
                username: ['', Validators.compose([Validators.nullValidator])],
                email: ['', Validators.compose([MyValidators.email])]
            });
        }
    }

    ngOnInit(): void {
        this.subscription = this.route.params.subscribe((params) => {
            this.uuid = params['uuid'];
            let _type = params['type'];
            this.full = this.uuid != null;
            this.email = _type === 'email';
            if (this.full) {
                let headers = new Headers();
                this.resetPassService
                    .submit(this.uuid)
                    .then((rsp) => {
                        this.username = rsp['headers'].get('username');
                    })
                    .catch(() => {
                        this.router.navigate([
                            '/msg',
                            {
                                type: 'passwordNotReset',
                                link: {
                                    route: ['login'],
                                    name: 'LOGIN_PAGE'
                                }
                            }
                        ]);
                    });
            }
        });
    }
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    public submit(event: any): boolean {
        event.preventDefault();
        if (!this.form.errors) {
            this.form.disable();
            this.resetPassService
                .reset(this.form.value)
                .then((rsp) => {
                    if (rsp.ok) {
                        if (this.full) {
                            this.router.navigate([
                                '/login',
                                {username: this.username}
                            ]);
                        } else {
                            this.router.navigate([
                                '/msg',
                                {
                                    type: 'emailSent',
                                    link: {
                                        route: ['login'],
                                        name: 'LOGIN_PAGE'
                                    }
                                }
                            ]);
                        }
                    } else {
                        this.router.navigate([
                            '/msg',
                            {
                                type: 'emailNotSent',
                                link: {
                                    route: ['resetPassword'],
                                    name: 'PASS_RESET'
                                }
                            }
                        ]);
                    }
                })
                .catch(() => {
                    this.form.enable();
                });
        }
        return false;
    }
}
