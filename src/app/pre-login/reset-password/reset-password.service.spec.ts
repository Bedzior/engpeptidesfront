import {TestBed, inject} from '@angular/core/testing';
import {HttpModule} from '@angular/http';

import {ResetPasswordService} from './reset-password.service';
import {AppConfigProvider} from 'app/common/app.config.provider';

describe('ResetPasswordService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ResetPasswordService, AppConfigProvider],
            imports: [HttpModule]
        });
    });

    it('should ...', inject(
        [ResetPasswordService],
        (service: ResetPasswordService) => {
            expect(service).toBeTruthy();
        }
    ));
});
