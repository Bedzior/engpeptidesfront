import {
    NgModule,
    CUSTOM_ELEMENTS_SCHEMA,
    APP_INITIALIZER,
    Injector
} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {
    CommonModule,
    APP_BASE_HREF,
    LOCATION_INITIALIZED
} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {RouterModule, Routes, RouteReuseStrategy} from '@angular/router';
import {StompRService} from '@stomp/ng2-stompjs';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateService} from '@ngx-translate/core';

import {AppComponent} from './app.component';
import {
    ResetPasswordServiceProvider,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    ResetPasswordComponent,
    RegisterServiceProvider
} from 'app/pre-login';
import {
    CrankComponent,
    AboutComponent,
    AboutPlaceholderComponent,
    SocketServiceProvider
} from 'app/common';
import {AppCommonModule} from 'app/common/common.module';
import {SearchConfigModule} from 'app/peptides-scanner/search-config/search-config.module';
import {AdminModule} from 'app/admin/admin.module';
import {IdentityServiceProvider} from './common/identity.service.provider';
import {WebpackTranslateLoader} from './webpack-translate-loader';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
        data: {
            title: 'APP.REDIRECTING'
        }
    },
    {
        path: 'register',
        component: RegisterComponent,
        data: {
            title: 'APP.REGISTRATION'
        }
    },
    {
        path: 'login',
        component: LoginComponent,
        data: {
            title: 'APP.LOGIN'
        }
    },
    {
        path: 'resetPassword',
        component: ResetPasswordComponent,
        data: {
            title: 'APP.PASS_RESET'
        }
    },
    {
        path: 'logout',
        component: LogoutComponent,
        data: {
            title: 'APP.LOGOUT'
        }
    },
    {
        path: 'about',
        component: AboutPlaceholderComponent,
        data: {
            title: 'APP.ABOUT'
        }
    }
];
export function appInitializerFactory(
    translate: TranslateService,
    injector: Injector
) {
    return () =>
        new Promise<any>((resolve: any) => {
            const locationInitialized = injector.get(
                LOCATION_INITIALIZED,
                Promise.resolve(null)
            );
            locationInitialized.then(() => {
                let browserLanguage = translate.getBrowserLang();
                let langToSet =
                    translate.langs.indexOf(browserLanguage) >= 0
                        ? browserLanguage
                        : 'en';
                translate.setDefaultLang('en');
                translate.use(langToSet).subscribe(
                    () => {
                        console.info(
                            `Successfully initialized language to '${langToSet}'`
                        );
                    },
                    (err) => {
                        console.error(
                            `Problem with language initialization: '${langToSet}' not set`
                        );
                    },
                    () => {
                        resolve(null);
                    }
                );
            });
        });
}

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        ResetPasswordComponent,
        LogoutComponent,
        CrankComponent,
        AboutComponent,
        AboutPlaceholderComponent
    ],
    imports: [
        BrowserModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useClass: WebpackTranslateLoader
            }
        }),
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule.forRoot(routes),
        MatProgressSpinnerModule,
        AppCommonModule,
        SearchConfigModule,
        AdminModule
    ],
    exports: [CrankComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    bootstrap: [AppComponent],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: appInitializerFactory,
            deps: [TranslateService, Injector],
            multi: true
        },
        StompRService,
        SocketServiceProvider,
        RegisterServiceProvider,
        IdentityServiceProvider,
        ResetPasswordServiceProvider,
        {
            provide: APP_BASE_HREF,
            useValue: '/'
        }
    ]
})
export class AppModule {}
