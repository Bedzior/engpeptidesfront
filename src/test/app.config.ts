import {ClassProvider} from '@angular/core';

import {IAppConfig} from 'app/common';

export class MockAppConfig implements IAppConfig {
    apiEndpoint: string;
    socketEndpoint: string;
    uploadEndpoint: string;
}

export const MockAppConfigProvider: ClassProvider = {
    provide: IAppConfig,
    useClass: MockAppConfig
};
