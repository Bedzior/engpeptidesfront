import {ClassProvider} from '@angular/core';

import {AdminConfigurationService, ConfigurationValueModel} from 'app/admin';

export class MockAdminConfigurationService
    implements AdminConfigurationService {
    listConfiguration(): Promise<ConfigurationValueModel[]> {
        throw new Error('Method not implemented.');
    }
    getConfig(name: string): Promise<ConfigurationValueModel> {
        throw new Error('Method not implemented.');
    }
    updateConfig(
        config: ConfigurationValueModel
    ): Promise<ConfigurationValueModel> {
        throw new Error('Method not implemented.');
    }
    updateConfigs(
        config: ConfigurationValueModel[]
    ): Promise<ConfigurationValueModel[]> {
        throw new Error('Method not implemented.');
    }
}

export const MockAdminConfigurationServiceProvider: ClassProvider = {
    provide: AdminConfigurationService,
    useClass: MockAdminConfigurationService
};
