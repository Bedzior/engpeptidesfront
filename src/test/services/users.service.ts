import {UsersService, UserEntity} from 'app/admin';
import {ClassProvider} from '@angular/core';

export class MockUsersService implements UsersService {
    listAll(): Promise<UserEntity[]> {
        return new Promise((resolve, reject) => {
            let response: UserEntity[] = [];
            response.push(
                {
                    id: 1,
                    login: 'abc',
                    email: 'doc@example.com',
                    activated: true,
                    admin: false
                },
                {
                    id: 2,
                    login: 'abcd',
                    email: 'docd@example.com',
                    activated: true,
                    admin: true
                }
            );
            resolve(response);
        });
    }
    update(user: UserEntity): Promise<UserEntity> {
        throw new Error('Method not implemented.');
    }
    remove(user: UserEntity): Promise<UserEntity> {
        throw new Error('Method not implemented.');
    }
}

export const MockUsersServiceProvider: ClassProvider = {
    provide: UsersService,
    useClass: MockUsersService
};
