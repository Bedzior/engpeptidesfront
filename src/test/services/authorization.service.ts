import {Subscription} from 'rxjs';
import {ClassProvider, Injectable} from '@angular/core';
import {Response} from '@angular/http';

import {AuthorizationService, UserModel} from 'app/common';

@Injectable()
export class MockAuthorizationService implements AuthorizationService {
    loggedIn: boolean;
    roles: Array<string>;
    error: any;
    data(arg0: any): any {
        throw new Error('Method not implemented.');
    }
    getUser(): UserModel {
        return new UserModel();
    }
    token(): string {
        return 'token';
    }
    getLogin(): string {
        return 'abc';
    }
    hasRole(role: string): boolean {
        return this.roles.indexOf(role) >= 0;
    }
    checkSession(): Promise<boolean> {
        return new Promise(() => {
            return true;
        });
    }
    subscribe(then: (isLogin: boolean) => void): Subscription {
        throw new Error('Method not implemented.');
    }
    constructor() {}

    public login(form: JSON): Promise<UserModel> {
        return new Promise((reject, response) => {
            if (this.loggedIn) {
                reject(new UserModel());
            } else {
                response(new UserModel());
            }
        });
    }

    public logout(): Promise<Response> {
        return new Promise((resolve, reject) => {
            if (this.error) {
                reject(this.data);
            } else {
                resolve();
            }
        });
    }
}

export const MockAuthorizationServiceProvider: ClassProvider = {
    provide: AuthorizationService,
    useClass: MockAuthorizationService
};
