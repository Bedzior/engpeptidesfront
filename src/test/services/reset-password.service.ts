import {Injectable, ClassProvider} from '@angular/core';
import {Response} from '@angular/http';
import {ResetPasswordService} from 'app/pre-login';

@Injectable()
export class MockResetPasswordService implements ResetPasswordService {
    reset(form: JSON): Promise<Response> {
        throw new Error('Method not implemented.');
    }
    submit(uuid: string): Promise<Response> {
        throw new Error('Method not implemented.');
    }
}

export const MockResetPasswordServiceProvider: ClassProvider = {
    provide: ResetPasswordService,
    useClass: MockResetPasswordService
};
