import {Observable} from 'rxjs';
import {ClassProvider} from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    CanActivateChild,
    RouterStateSnapshot
} from '@angular/router';

import {LoggedInGuard} from 'app/common';

export class MockLoggedInGuard implements CanActivate, CanActivateChild {
    canActivateChild(
        childRoute: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean> {
        throw new Error('Method not implemented.');
    }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean> {
        throw new Error('Method not implemented.');
    }
}
export const MockLoggedInGuardProvider: ClassProvider = {
    provide: LoggedInGuard,
    useClass: MockLoggedInGuard
};
