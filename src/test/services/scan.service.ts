import {Observable} from 'rxjs';
import {ClassProvider, Injectable} from '@angular/core';
import {Response} from '@angular/http';

import {ScanModel, ScanService, ScanStatus} from 'app/peptides-scanner';
import {
    MsOutputModel,
    MsPeptideHit,
    MsProteinHit
} from 'app/peptides-scanner/scan-list/scan-element/ms-output-model';

@Injectable()
export class MockScanService implements ScanService {
    getPeptides(token: string, protein: string): Promise<Array<MsPeptideHit>> {
        return new Promise(() => {
            let result: MsPeptideHit[];
            result.push({
                sequence: 'ABCD',
                calcMass: '21.01934242',
                queriesCount: 0,
                queries: []
            });
            return result;
        });
    }
    getProteins(token: string): Promise<Array<MsProteinHit>> {
        return new Promise(() => {
            return [];
        });
    }
    start(form: any): Promise<Response> {
        throw new Error('Method not implemented.');
    }
    list(): Promise<ScanModel[]> {
        return new Promise((response, reject) => {
            let models: ScanModel[] = [];
            models.push(
                new ScanModel({
                    token: '',
                    status: ScanStatus.RUNNING,
                    parameters: '',
                    created: new Date(),
                    started: new Date(),
                    finished: new Date()
                })
            );
            response(models);
        });
    }
    listRunning(): Promise<ScanModel[]> {
        throw new Error('Method not implemented.');
    }
    isRunningOrWaiting(token: string): Observable<{}> {
        throw new Error('Method not implemented.');
    }
    get(token: string): Promise<ScanModel> {
        throw new Error('Method not implemented.');
    }
    public listen(): Observable<{}> {
        return Observable.create((obs) => {
            obs.next({message: 'Lorem ipsum'});
            return () => {};
        });
    }
    getResult(token: string): Promise<MsOutputModel> {
        throw new Error('Method not implemented.');
    }
    cancel(token: string): Promise<Response> {
        throw new Error('Method not implemented.');
    }
}

export const MockScanServiceProvider: ClassProvider = {
    provide: ScanService,
    useClass: MockScanService
};
