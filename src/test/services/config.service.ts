import {ScanConfigurationService} from 'app/peptides-scanner';
import {ClassProvider} from '@angular/core';

export class MockScanConfigurationService implements ScanConfigurationService {
    getOptions(advanced?: boolean): Promise<{}> {
        return new Promise((resolve, reject) => {
            resolve({});
        });
    }
}

export const MockScanConfigurationServiceProvider: ClassProvider = {
    provide: ScanConfigurationService,
    useClass: MockScanConfigurationService
};
