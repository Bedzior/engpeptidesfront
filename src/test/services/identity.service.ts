import {Injectable, ClassProvider} from '@angular/core';
import {IdentityService} from 'app/common';

@Injectable()
export class MockIdentityService implements IdentityService {
    update(form: {}): Promise<{}> {
        throw new Error('Method not implemented.');
    }
}
export const MockIdentityServiceProvider: ClassProvider = {
    provide: IdentityService,
    useClass: MockIdentityService
};
