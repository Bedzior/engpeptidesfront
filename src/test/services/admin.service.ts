import {ClassProvider} from '@angular/core';
import {AdminService, ServerStatus} from 'app/admin';

export class MockAdminServiceImpl implements AdminService {
    getStatus(): Promise<ServerStatus> {
        return new Promise((resolve, reject) => {
            resolve({
                scanCount: 5,
                waitingScanCount: 10
            });
        });
    }
}

export const MockAdminServiceProvider: ClassProvider = {
    provide: AdminService,
    useClass: MockAdminServiceImpl
};
