import {Injectable, ClassProvider} from '@angular/core';
import {Response} from '@angular/http';

import {RegisterService} from 'app/pre-login';

@Injectable()
export class MockRegisterService implements RegisterService {
    register(form: JSON, withHeaders?: boolean): Promise<Response> {
        throw new Error('Method not implemented.');
    }
    confirm(uuid: string, withHeaders?:boolean): Promise<Response> {
        throw new Error('Method not implemented.');
    }
}

export const MockRegisterServiceProvider: ClassProvider = {
    provide: RegisterService,
    useClass: MockRegisterService
};
