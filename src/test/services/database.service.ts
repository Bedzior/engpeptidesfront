import {Observable} from 'rxjs';

import {DatabaseService, DatabaseElementModel} from 'app/admin/databases';
import {ClassProvider} from '@angular/core';

export class MockDatabaseService implements DatabaseService {
    list(): Promise<DatabaseElementModel[]> {
        return new Promise<DatabaseElementModel[]>((resolve, reject) => {
            resolve([
                new DatabaseElementModel({id: 1, name: 'abc'}),
                new DatabaseElementModel({id: 2, name: 'cde'})
            ]);
        });
    }
    create(
        form: DatabaseElementModel
    ): Promise<DatabaseElementModel> | Observable<{}> {
        throw new Error('Method not implemented.');
    }
    unsubscribe() {
    }
    subscribe(name: string): Observable<{}> {
        throw new Error('Method not implemented.');
    }
    update(form: DatabaseElementModel): Promise<DatabaseElementModel> {
        throw new Error('Method not implemented.');
    }
    cancel(name: string) {
        throw new Error('Method not implemented.');
    }
    remove(id: number): Promise<number> {
        throw new Error('Method not implemented.');
    }
}
export const MockDatabaseServiceProvider: ClassProvider = {
    provide: DatabaseService,
    useClass: MockDatabaseService
};
