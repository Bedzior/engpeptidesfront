import { Observable } from 'rxjs';

import { ClassProvider } from '@angular/core';
import { SocketService } from 'app/common';

export class MockSocketServiceImpl implements SocketService {
    disconnect() {
        throw new Error('Method not implemented.');
    }
    subscribe(queue: string): Observable<{}> {
        throw new Error('Method not implemented.');
    }
    unsubscribe() {
        throw new Error('Method not implemented.');
    }
    publish(queue: string, body: {}) {
        throw new Error('Method not implemented.');
    }
}

export const MockSocketServiceProvider: ClassProvider = {
    provide: SocketService,
    useClass: MockSocketServiceImpl
};
