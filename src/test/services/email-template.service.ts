import {ClassProvider} from '@angular/core';

import {EmailTemplateModel, EmailTemplateService} from 'app/admin';

export class MockEmailTemplateService implements EmailTemplateService {
    list(): Promise<EmailTemplateModel[]> {
        return new Promise((resolve, reject) => {
            let response: EmailTemplateModel[] = [];
            response.push(
                new EmailTemplateModel({
                    id: 1,
                    name: 'someName',
                    type: 'html',
                    body: '<p>There you go</p>'
                })
            );
            resolve(response);
        });
    }
    getSingle(id: number): Promise<EmailTemplateModel> {
        throw new Error('Method not implemented.');
    }
    update(item: EmailTemplateModel): Promise<EmailTemplateModel> {
        throw new Error('Method not implemented.');
    }
}
export const MockEmailTemplateServiceProvider: ClassProvider = {
    provide: EmailTemplateService,
    useClass: MockEmailTemplateService
};
