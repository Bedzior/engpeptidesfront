module.exports = function(config) {
	config.set({
		basePath : '',
		frameworks : [ 'jasmine', '@angular/cli' ],
		plugins : [ require('karma-jasmine'),
				require('karma-typescript'),
				require('karma-firefox-launcher'),
				require('karma-chrome-launcher'),
				require('karma-edge-launcher'),
				require('karma-remap-istanbul'),
				require('@angular/cli/plugins/karma') ],
		files : [ {
			pattern : './node_modules/@angular/material/prebuilt-themes/deeppurple-amber.css',
			included: true
		}],
		preprocessors : {
			'./src/test.ts' : [ '@angular/cli' ]
		},
		mime : {
			'text/x-typescript' : [ 'ts', 'tsx' ]
		},
		remapIstanbulReporter : {
			reports : {
				html : 'coverage',
				lcovonly : './coverage/coverage.lcov'
			}
		},
		angularCli : {
			config : './angular-cli.json',
			environment : 'dev'
		},
		reporters : config.angularCli && config.angularCli.codeCoverage ? [
				'progress', 'karma-remap-istanbul' ] : [ 'progress' ],
		port : 9876,
		colors : true,
		logLevel : config.LOG_INFO,
		autoWatch : true,
		browsers : [ 'Chrome', 'Firefox', 'Edge' ],
		// browsers : [ 'Chrome' ],
		singleRun : false
	});
};
