const PROXY_CONFIG = [{
	"context": ["/api/ws"],
	"target": "http://localhost:8080",
	"secure": false,
	"bypass": function (req, rsp) {
		console.log("Socket connection request")
	},
	"changeOrigin": "true",
	"ws": true
}, {
	"context": ["/api/**"],
	"target": "http://localhost:8080",
	"secure": false,
	"bypass": function (req, rsp) {
		console.log("Proxying " + req.url)
	},
	"changeOrigin": "true"
}]

module.exports = PROXY_CONFIG;