import * as p from 'protractor';

export class FrontendPage {

    waitForRedirect(url: string) {
        p.browser.debugger();
        return p.browser.driver.wait(() => {
            return p.browser.driver.getCurrentUrl().then((current) => {
                if (!current.endsWith(url)) {
                    throw 'Not the page we expected: is ' + current + '; ' + url + ' wanted';
                }
                return current;
            });
        });
    }

    navigateTo() {
        return p.browser.get('/');
    }

    getParagraphText() {
        return p.element(p.by.css('.floatbox h1')).getText();
    }

    getFloatbox(): p.ElementFinder {
        return p.element(p.by.css('.floatbox'));
    }

}
