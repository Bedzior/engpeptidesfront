import {FrontendPage} from './app.po';
import {by} from 'protractor';

describe('frontend App', function() {
    var page: FrontendPage;

    beforeEach(() => {
        page = new FrontendPage();
    });

    it('should display message with the app title', async function() {
        page.navigateTo();
        expect(await page.getParagraphText()).toBe('peptides');
    });

    it('should contain a login form, as no user sessions is saved', async () => {
        page.navigateTo();
        expect(
            await page.getFloatbox().element(by.tagName('form'))
        ).toBeTruthy();
    });

    it('should display an error if a login attempt did not succeed', async () => {
        page.navigateTo();
        let form = page.getFloatbox().element(by.tagName('form'));
        form.element(by.tagName('input'))
            .sendKeys('someUser')
            .then(() => {
                form.element(by.css('[type="password"]'))
                    .sendKeys('somePassword')
                    .then(() => {
                        form.element(by.css('[type="submit"]'))
                            .click()
                            .then(() => {
                                expect(
                                    form.element(by.css('.error'))
                                ).toBeTruthy();
                            });
                    });
            });
    });

    it('should login the administrative user', async () => {
        pending('Skipping without mock http')
        page.navigateTo();
        let form = page.getFloatbox().element(by.tagName('form'));
        form.element(by.css('[name="j_username"]'))
            .sendKeys('admin')
            .then(() => {
                form.element(by.css('[name="j_password"]'))
                    .sendKeys('admin')
                    .then(() => {
                        form.element(by.css('[type="submit"]'))
                            .click()
                            .then(() => {
                                page.getFloatbox()
                                    .element(by.tagName('form'))
                                    .getAttribute('class')
                                    .then((val) => {
                                        expect(
                                            page.waitForRedirect(
                                                'config/simple'
                                            )
                                        ).toBeTruthy();
                                    });
                            });
                    });
            });
    });
});
