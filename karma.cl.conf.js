module.exports = function(config) {
	config.set({
		basePath : '',
		frameworks : [ 'jasmine', '@angular/cli' ],
		plugins : [ require('karma-jasmine'),
				require('karma-chrome-launcher'),
				require('karma-remap-istanbul'),
				require('@angular/cli/plugins/karma') ],
		files : [ {
			pattern : './src/test.ts',
			watched : true
		}, {
			pattern : './node_modules/@angular/material/prebuilt-themes/deeppurple-amber.css',
			included: true
		}  ],
		preprocessors : {
			'./src/test.ts' : [ '@angular/cli' ]
		},
		mime : {
			'text/x-typescript' : [ 'ts', 'tsx' ]
		},
		remapIstanbulReporter : {
			reports : {
				html : 'coverage',
				lcovonly : './coverage/coverage.lcov'
			}
		},
		angularCli : {
			config : './angular-cli.json',
			environment : 'dev'
		},
		reporters : config.angularCli && config.angularCli.codeCoverage ? [
				'progress', 'karma-remap-istanbul' ] : [ 'progress' ],
		port : 9876,
		colors : false,
		logLevel : config.LOG_WARN,
		autoWatch : false,
		browsers: ['ChromeNoSandbox'],
	    customLaunchers: {
	      ChromeNoSandbox: {
	    	// TODO ensure a headless browser for windowless environments
	    	base: 'ChromeHeadless',
	        flags: [
	        	'--no-sandbox',
	        	'--headless',
	            '--disable-gpu',
	            ' --remote-debugging-port=9222'
	            ]
	      }
	    },
		singleRun : true
	});
};
